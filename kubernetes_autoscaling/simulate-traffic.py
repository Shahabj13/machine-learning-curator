import requests
import threading

REQUEST_STRING = "http://192.168.49.2:30100/images/dog.jpeg"
MAX_NUM_THREAD = 3
MAX_ITERATIONS = 10000

def spamRequests():
    i = 0
    while i < MAX_ITERATIONS:
        requests.get(REQUEST_STRING)
        i = i + 1

for i in range(MAX_NUM_THREAD):
    threading.Thread(target=spamRequests())
