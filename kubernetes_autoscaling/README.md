# Kubernetes Image Server Horizontal Pod Autoscaling using Minikube #

### Setup Guide ###

STEPS:
1. Run the setup.sh script to install Kubectl and Minikube
2. Run the following commands:
    2.1 eval $(minikube -p minikube docker-env)
    2.2 docker build ../Image_Server_API -t image-server/image-server
    2.3 kubectl apply -f persistent-volume.yaml - creates volume on local directory in node VM
    2.4 kubectl apply -f persistent-volume-claim.yaml - claims a portion of the persistent volume for use
    2.5 kubectl apply -f image-server.yaml - defines image server pod deployment
    2.6 kubectl apply -f image-server-hpa.yaml - creates Horizontal Pod Autoscaler which is responsible for creation/deletion of pods depending on demand
    2.7 kubectl apply -f image-server-svc.yaml - exposes a LoadBalancer IP that can be used to access the underlying image server API
    2.8 minikube start
3.	Can now access the image server API via “http://192.168.49.2:30100”


### API ###
See User & Installation guide or README in root directory of this project for detailsr regarding the Image Server API.