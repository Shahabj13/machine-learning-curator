from flask import Flask, request, render_template
from contextlib import asynccontextmanager
import threading

import base64
import json
from detect import inference

import requests
import os
import glob
from pathlib import Path
import train

app = Flask(__name__)

TRAINING_FOLDER = '/training_data/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'bmp', 'tif', 'tiff', 'webp'}

preparingDataset = False

# Check if image file is of a supported type
def allowedFile(filename):
    extension = filename.rsplit('.', 1)[1].lower()
    return True if ('.' in filename and extension in ALLOWED_EXTENSIONS) else False

# Retrieve list of labels from given data
def getLabels(images):
    labelList = []
    for image in images:
        for annotation in image['annotations']:
            labelList.append(annotation['label'])
    labelList = list(set(labelList)) # remove duplicate labels
    return labelList

# Retrieve images from image server and create annotation files
def getImages(images, index, labelList, imageServer, validation):
    for i, image in enumerate(images[index:]):
        filename = image['filename']
        
        # Check if file is of a supported type
        if (not allowedFile(filename)):
            continue
        
        # Try to download file from image server
        imageData = ''
        imageDecode = ''
        try:
            imageData = requests.get(imageServer + filename).json()
        except:
            continue
        
        if (imageData['response'] == 'success'):
        
            # Decode and save image file
            imageDecode = base64.b64decode(imageData['image'])
            with open(TRAINING_FOLDER + 'retraining/dataset/' + filename, 'wb') as imageFile:
                imageFile.write(imageDecode)
            
            # Save associated annotations file
            annotations = image['annotations']
            with open(TRAINING_FOLDER + 'retraining/dataset/' + Path(filename).stem + ".txt", 'w') as annotationsFile:
                for annotation in annotations:
                    labelID = labelList.index(annotation['label'])
                    annotationsFile.write(str(labelID) + ' ' + annotation['bounding_box'] + '\n')
            
            # If performing dataset validation
            if (validation):
                return i+1 if (i < len(images)-1) else 0
            
    return -1

# Create dataset.yaml file
def defineDataset(labelList):
    with open('/training_data/dataset.yaml', 'w') as datasetFile:
        datasetFile.write("path: './'\n" \
            + "train: '" + TRAINING_FOLDER + "retraining/dataset/'\n" \
            + "val: '" + TRAINING_FOLDER + "retraining/dataset/'\n\n" \
            + "nc: " + str(len(labelList)) + "\n" \
            + "names: [")
        for i, label in enumerate(labelList):
            if (i + 1 != len(labelList)):
                datasetFile.write(" '" + label + "', ")
            else:
                datasetFile.write(" '" + label + "' ]")
    return True

# Download remaining images and start training
def startTraining(images, index, labelList, imageServer, finetune, epochs):
    global preparingDataset
    preparingDataset = True
    
    if (index != 0):
        getImages(images, index, labelList, imageServer, False)
    
    preparingDataset = False
    
    # Start training
    train.start(epochs, finetune, TRAINING_FOLDER)

# Detect objects in a given image
@app.route('/detect', methods=['POST'])
def detect():
    data = request.get_json() # receive POST data
    imagedata = base64.b64decode(data['image']) # decode base64 data
    return inference(imagedata) # send to inference script

# Return current training progress for ML model
@app.route('/progress', methods=['GET'])
def trainProgress():
    if (preparingDataset):
        return {"progress":"preparing dataset"}
    return {"progress":train.progress} if train.status else {"progress":"stopped"}

# Return training status of ML model
@app.route('/status', methods=['GET'])
def trainStatus():
    return {"status":"running"} if (train.status or preparingDataset) else {"status":"stopped"}

# Start training or finetuning the ML model using a given image server, filenames, and annotations
@app.route('/train', methods=['POST'])
def trainML():
    if (train.status):
        return {"response":"error","message":"training already running"}
        
    data = request.get_json()
    
    finetune = data['finetune']
    if (finetune):
        # If model is not already custom trained, return an error
        if (not os.path.isfile(TRAINING_FOLDER + 'finetune/dataset.json')):
            return {"response":"error","message":"model has not been custom trained"}
    
    imageServer = data['image_server']
    images = data['images']
    
    # If finetuning, add past dataset to current one
    if (finetune):
        with open(TRAINING_FOLDER + 'finetune/dataset.json', 'r') as f:
            pastData = json.load(f)
            pastImages = pastData['images']
            
            # Remove duplicate files from old dataset
            for image in images:
                # Iterate backwards to allow for pop
                for i in range(len(pastImages) -1, -1, -1):
                    if (pastImages[i]['filename'] == image['filename']):
                        pastImages.pop(i)
                    
            images = images + pastImages
            
    labelList = getLabels(images)
    
    # Remove leftover dataset files from previous training (in case training stopped unexpectedly)
    pastTrainingData = glob.glob(TRAINING_FOLDER + 'retraining/dataset/*')
    for file in pastTrainingData:
        os.remove(file)
    
    # Attempt to download images until successful
    validation = getImages(images, 0, labelList, imageServer, True)
    
    if (validation < 0):
        return {"response":"error","message":"no images could be downloaded"}
    
    epochs = data['epochs']
    
    # Create dataset.yaml
    defineDataset(labelList)
    
    # Write new dataset to finetune history and backup old history
    if (finetune):
        os.rename(TRAINING_FOLDER + 'finetune/dataset.json', TRAINING_FOLDER + 'finetune/dataset_backup.json')
    with open(TRAINING_FOLDER + 'finetune/dataset.json', 'w') as f:
         json.dump(data, f)
    
    # Start background thread to download remaining images and start training
    thread = threading.Thread(target=startTraining, args=[images, validation, labelList, imageServer, finetune, epochs])
    thread.daemon = True
    thread.start()
    
    if (finetune):
        return {"response":"success","message":"finetuning initiated"}

    return {"response":"success","message":"training initiated"}

if __name__ == '__main__':

    # Create docker volume folders if not already present
    if (not os.path.exists('/training_data/retraining')):
        os.makedirs('/training_data/retraining')
    if (not os.path.exists('/training_data/retraining/dataset')):
        os.makedirs('/training_data/retraining/dataset')
    if (not os.path.exists('/training_data/retraining/models')):
        os.makedirs('/training_data/retraining/models')
    if (not os.path.exists('/training_data/backups')):
        os.makedirs('/training_data/backups')
    if (not os.path.exists('/training_data/finetune')):
        os.makedirs('/training_data/finetune')

    app.run(host='0.0.0.0', port=9000)
