# -*- coding: utf-8 -*-
# YOLOv5 🚀 by Ultralytics, GPL-3.0 license

import os
import glob
import sys
from copy import deepcopy
from pathlib import Path
from datetime import datetime

import numpy as np
import torch
import torch.distributed as dist
import torch.nn as nn
import yaml
from torch.cuda import amp
from torch.optim import SGD, lr_scheduler
from tqdm import tqdm

FILE = Path(__file__).absolute()
sys.path.append(FILE.parents[0].as_posix())  # add yolov5/ to path

from models.yolo import Model
from utils.autoanchor import check_anchors
from utils.datasets import create_dataloader
from utils.general import labels_to_class_weights, init_seeds, strip_optimizer, \
    check_dataset, check_img_size, one_cycle, colorstr
from utils.loss import ComputeLoss
from utils.torch_utils import ModelEMA, select_device, intersect_dicts, torch_distributed_zero_first, de_parallel
from utils.metrics import fitness
from utils.callbacks import Callbacks

LOCAL_RANK = int(os.getenv('LOCAL_RANK', -1))  # https://pytorch.org/docs/stable/elastic/run.html
RANK = int(os.getenv('RANK', -1))
WORLD_SIZE = int(os.getenv('WORLD_SIZE', 1))

FREEZE_LAYERS = 10 # how many weights layers to freeze for finetuning. Backbone = 10, all = 24
IMAGE_CACHE = "" # optional location to cache training images, can be set to "ram" or "disk"
DEVICE = "" # CUDA device. Defaults to CPU. Can be set to 0 or 0,1,2,3 or cpu
IMAGE_SIZE = 640 # Image size in pixels for training
BATCH_SIZE = 16 # Image batch size for training. Must be multiple of CUDA device count
WEIGHTS = "foundation/foundation.pt" # Initial weights to train from if no custom trained model is present

progress = ''
status = False

def train(device,
          epochs,
          finetune,
          trainingFolder,
          callbacks=Callbacks()
          ):
    # Attempt training
    try:
        # Directories
        saveFolder = Path(trainingFolder + 'retraining/models') # temporary folder to save training models
        last, best = saveFolder / 'last.pt', saveFolder / 'best.pt'

        # Hyperparameters
        with open("hyp.yaml") as f:
            hyp = yaml.safe_load(f)  # load hyps dict

        data_dict = None

        # Config
        data = trainingFolder + "dataset.yaml"
        cuda = device.type != 'cpu'
        init_seeds(1 + RANK)
        with torch_distributed_zero_first(RANK):
            data_dict = check_dataset(data)
        train_path, val_path = data_dict['train'], data_dict['val']
        nc = int(data_dict['nc'])  # number of classes
        names = data_dict['names']  # class names
        assert len(names) == nc, f'{len(names)} names found for nc={nc} dataset in {data}'  # check

        # Model
        pretrained = (os.path.isfile(WEIGHTS))
        if (pretrained): # If weights exist
            with torch_distributed_zero_first(RANK):
                weights = WEIGHTS
            ckpt = torch.load(weights, map_location=device)  # load checkpoint
            model = Model(ckpt['model'].yaml, ch=3, nc=nc, anchors=hyp.get('anchors')).to(device)  # create
            exclude = ['anchor'] if (hyp.get('anchors')) else []  # exclude keys
            csd = ckpt['model'].float().state_dict()  # checkpoint state_dict as FP32
            csd = intersect_dicts(csd, model.state_dict(), exclude=exclude)  # intersect
            model.load_state_dict(csd, strict=False)  # load
        else: # Randomise weights
            model = Model('models/yolov5s.yaml', ch=3, nc=nc, anchors=hyp.get('anchors')).to(device)  # create
        
        
        # Freeze
        if (finetune):
            freeze = [f'model.{x}.' for x in range(FREEZE_LAYERS)]  # layers to freeze
        else:
            freeze = [f'model.{x}.' for x in range(0)]  # don't freeze
        for k, v in model.named_parameters():
            v.requires_grad = True  # train all layers
            if any(x in k for x in freeze):
                print(f'freezing {k}')
                v.requires_grad = False

        # Optimizer
        nbs = 64  # nominal batch size
        accumulate = max(round(nbs / BATCH_SIZE), 1)  # accumulate loss before optimizing
        hyp['weight_decay'] *= BATCH_SIZE * accumulate / nbs  # scale weight_decay

        g0, g1, g2 = [], [], []  # optimizer parameter groups
        for v in model.modules():
            if hasattr(v, 'bias') and isinstance(v.bias, nn.Parameter):  # bias
                g2.append(v.bias)
            if isinstance(v, nn.BatchNorm2d):  # weight (no decay)
                g0.append(v.weight)
            elif hasattr(v, 'weight') and isinstance(v.weight, nn.Parameter):  # weight (with decay)
                g1.append(v.weight)

        optimizer = SGD(g0, lr=hyp['lr0'], momentum=hyp['momentum'], nesterov=True)

        optimizer.add_param_group({'params': g1, 'weight_decay': hyp['weight_decay']})  # add g1 with weight_decay
        optimizer.add_param_group({'params': g2})  # add g2 (biases)
        del g0, g1, g2

        # Scheduler
        lf = one_cycle(1, hyp['lrf'], epochs)  # cosine 1->hyp['lrf']
        scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)  # plot_lr_scheduler(optimizer, scheduler, epochs)

        # EMA
        ema = ModelEMA(model) if RANK in [-1, 0] else None

        # Resume
        start_epoch, best_fitness = 0, 0.0
        if (pretrained):
            # Optimizer
            if ckpt['optimizer'] is not None:
                optimizer.load_state_dict(ckpt['optimizer'])
                best_fitness = ckpt['best_fitness']

            # EMA
            if ema and ckpt.get('ema'):
                ema.ema.load_state_dict(ckpt['ema'].float().state_dict())
                ema.updates = ckpt['updates']

            # Epochs
            start_epoch = ckpt['epoch'] + 1
            if epochs < start_epoch:
                epochs += ckpt['epoch']  # finetune additional epochs

            del ckpt, csd

        # Image sizes
        gs = max(int(model.stride.max()), 32)  # grid size (max stride)
        nl = model.model[-1].nl  # number of detection layers (used for scaling hyp['obj'])
        imgsz = check_img_size(IMAGE_SIZE, gs, floor=gs * 2)  # verify imgsz is gs-multiple

        # Trainloader
        train_loader, dataset = create_dataloader(train_path, imgsz, BATCH_SIZE // WORLD_SIZE, gs, False,
                                                  hyp=hyp, augment=True, cache=IMAGE_CACHE, rank=RANK,
                                                  workers=8, quad=False,
                                                  prefix=colorstr('train: '))
        mlc = int(np.concatenate(dataset.labels, 0)[:, 0].max())  # max label class
        nb = len(train_loader)  # number of batches
        assert mlc < nc, f'Label class {mlc} exceeds nc={nc} in {data}. Possible class labels are 0-{nc - 1}'

        # Process 0
        if RANK in [-1, 0]:
            val_loader = create_dataloader(val_path, imgsz, BATCH_SIZE // WORLD_SIZE * 2, gs, False,
                                           hyp=hyp, cache=None,
                                           workers=8, pad=0.5,
                                           prefix=colorstr('val: '))[0]

            labels = np.concatenate(dataset.labels, 0)

            # Anchors
            check_anchors(dataset, model=model, thr=hyp['anchor_t'], imgsz=imgsz)
            model.half().float()  # pre-reduce anchor precision

            callbacks.on_pretrain_routine_end()


        # Model parameters
        hyp['box'] *= 3. / nl  # scale to layers
        hyp['cls'] *= nc / 80. * 3. / nl  # scale to classes and layers
        hyp['obj'] *= (imgsz / 640) ** 2 * 3. / nl  # scale to image size and layers
        hyp['label_smoothing'] = 0.0
        model.nc = nc  # attach number of classes to model
        model.hyp = hyp  # attach hyperparameters to model
        model.class_weights = labels_to_class_weights(dataset.labels, nc).to(device) * nc  # attach class weights
        model.names = names

        # Start training
        nw = max(round(hyp['warmup_epochs'] * nb), 1000)  # number of warmup iterations, max(3 epochs, 1k iterations)
        last_opt_step = -1
        maps = np.zeros(nc)  # mAP per class
        results = (0, 0, 0, 0, 0, 0, 0)  # P, R, mAP@.5, mAP@.5-.95, val_loss(box, obj, cls)
        scheduler.last_epoch = start_epoch - 1  # do not move
        scaler = amp.GradScaler(enabled=cuda)
        compute_loss = ComputeLoss(model)  # init loss class
        
        global progress
        progress = ''
        
        for epoch in range(start_epoch, epochs):  # epoch ------------------------------------------------------------------
            model.train()

            mloss = torch.zeros(3, device=device)  # mean losses
            if RANK != -1:
                train_loader.sampler.set_epoch(epoch)
            pbar = enumerate(train_loader)

            optimizer.zero_grad()

            for i, (imgs, targets, paths, _) in pbar:  # batch -------------------------------------------------------------
                ni = i + nb * epoch  # number integrated batches (since train start)
                progress = "Epoch: " + str(epoch+1) + " of " + str(epochs) + ", Batch " + str(i+1) + " of " + str(nb)
                
                imgs = imgs.to(device, non_blocking=True).float() / 255.0  # uint8 to float32, 0-255 to 0.0-1.0

                # Warmup
                if ni <= nw:
                    xi = [0, nw]  # x interp
                    accumulate = max(1, np.interp(ni, xi, [1, nbs / BATCH_SIZE]).round())
                    for j, x in enumerate(optimizer.param_groups):
                        # bias lr falls from 0.1 to lr0, all other lrs rise from 0.0 to lr0
                        x['lr'] = np.interp(ni, xi, [hyp['warmup_bias_lr'] if j == 2 else 0.0, x['initial_lr'] * lf(epoch)])
                        if 'momentum' in x:
                            x['momentum'] = np.interp(ni, xi, [hyp['warmup_momentum'], hyp['momentum']])

                # Forward
                with amp.autocast(enabled=cuda):
                    pred = model(imgs)  # forward
                    loss, loss_items = compute_loss(pred, targets.to(device))  # loss scaled by batch_size
                    if RANK != -1:
                        loss *= WORLD_SIZE  # gradient averaged between devices in DDP mode

                # Backward
                scaler.scale(loss).backward()

                # Optimize
                if ni - last_opt_step >= accumulate:
                    scaler.step(optimizer)  # optimizer.step
                    scaler.update()
                    optimizer.zero_grad()
                    if ema:
                        ema.update(model)
                    last_opt_step = ni

                # end batch ------------------------------------------------------------------------------------------------

            # Scheduler
            scheduler.step()

            if RANK in [-1, 0]:
                # mAP
                callbacks.on_train_epoch_end(epoch=epoch)
                ema.update_attr(model, include=['yaml', 'nc', 'hyp', 'names', 'stride', 'class_weights'])
                final_epoch = epoch + 1 == epochs

                # Update best mAP
                fi = fitness(np.array(results).reshape(1, -1))  # weighted combination of [P, R, mAP@.5, mAP@.5-.95]
                if fi > best_fitness:
                    best_fitness = fi

                # Save model
                ckpt = {'epoch': epoch,
                        'best_fitness': best_fitness,
                        'model': deepcopy(de_parallel(model)).half(),
                        'ema': deepcopy(ema.ema).half(),
                        'updates': ema.updates,
                        'optimizer': optimizer.state_dict(),
                        'wandb_id': None}

                # Save last, best and delete
                torch.save(ckpt, last)
                if best_fitness == fi:
                    torch.save(ckpt, best)
                del ckpt
                callbacks.on_model_save(last, epoch, final_epoch, best_fitness, fi)

            # end epoch ----------------------------------------------------------------------------------------------------
        # end training -----------------------------------------------------------------------------------------------------
        if RANK in [-1, 0]:
            # Strip optimizers
            for f in last, best:
                if f.exists():
                    strip_optimizer(f)  # strip optimizers
            callbacks.on_train_end(last, best, False, epoch)

        torch.cuda.empty_cache()
        
        # Delete image, annotation, and unneeded model files
        trainingData = glob.glob(train_path + '/*')
        for file in trainingData:
            os.remove(file)
        os.remove(trainingFolder + 'retraining/models/last.pt')
        
        # Backup and replace old model
        if (os.path.isfile(trainingFolder + 'model.pt')):
            today = datetime.now()
            today = today.strftime("%b-%d-%Y--%H-%M-%S")
            os.rename(trainingFolder + 'model.pt', trainingFolder + 'backups/' + today + '.pt')
        os.rename(trainingFolder + 'retraining/models/best.pt', trainingFolder + 'model.pt')
        
    # Clean up if an exception occurs during training
    except Exception as e:
        print(e)
        trainingData = glob.glob(train_path + '/*')
        for file in trainingData:
            os.remove(file)
        modelData = glob.glob(trainingFolder + 'retraining/models/*')
        for file in modelData:
            os.remove(file)
        
        # Delete new finetune history and restore backup history
        if (finetune):
            os.remove(trainingFolder + 'finetune/dataset.json')
            os.rename(trainingFolder + 'finetune/dataset_backup.json', trainingFolder + 'finetune/dataset.json')
    
    global status
    status = False
    return
        

def start(epochs, finetune, trainingFolder):
    global status
    status = True
    global progress
    progress = "Starting..."
    # DDP mode
    device = select_device(DEVICE, batch_size=BATCH_SIZE)
    if LOCAL_RANK != -1:
        assert torch.cuda.device_count() > LOCAL_RANK, 'insufficient CUDA devices for DDP command'
        assert BATCH_SIZE % WORLD_SIZE == 0, '--batch-size must be multiple of CUDA device count'
        torch.cuda.set_device(LOCAL_RANK)
        device = torch.device('cuda', LOCAL_RANK)
        dist.init_process_group(backend="nccl" if dist.is_nccl_available() else "gloo")

    # Train
    train(device, epochs, finetune, trainingFolder)
    if WORLD_SIZE > 1 and RANK == 0:
        _ = [print('Destroying process group... ', end=''), dist.destroy_process_group(), print('Done.')]


