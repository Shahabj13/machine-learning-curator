#!/bin/bash

function getModelChoice() {
    echo "Select the starting ML model:"
    echo "1)Default YOLOv5s model"
    echo "2)Custom model"
    echo "3)No model (randomised weights)"

    read -n 1 opt

    case $opt in
    "1")
        echo "You chose the default model"
        sudo docker build -t machine_learning_api .
        ;;
    "2")
        echo "You chose the custom model"
        echo "What is the model URL?"
        read modelURL
        sudo docker build --build-arg FOUNDATIONMODEL=$modelURL -t machine_learning_api .
        ;; 
    "3")
        echo "You chose no model"
        sudo docker build --build-arg FOUNDATION=False -t machine_learning_api .
        ;;
    *) echo "Invalid option"
        getModelChoice
        ;;
    esac
}

getModelChoice

echo "Running container..."
sudo docker run -d --name ml_api --shm-size=4g --network host machine_learning_api