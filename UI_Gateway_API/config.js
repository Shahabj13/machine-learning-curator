const dotenv = require('dotenv');
dotenv.config();

const configure = {
    development: {
        client: 'pg',
        database: process.env.DB_NAME,
        user: process.env.DB_USER,
        host: 'localhost',
        password: process.env.DB_PASS,
        port: 5432,
        image_server_ip: process.env.IMAGE_SERVER_IP,
        image_server_port: process.env.IMAGE_SERVER_PORT,
        ml_server_ip: process.env.ML_SERVER_IP,
        ml_server_port: process.env.ML_SERVER_PORT,
        ssl: {
            rejectUnauthorized: false
        },
        //server details
        server: {
            host: '127.0.0.1',
            port: process.env.APP_PORT
        },
    },
};

module.exports = configure;