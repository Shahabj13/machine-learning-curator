const request = require('request');
const dotenv = require('dotenv');
dotenv.config();
var env = process.env.NODE_ENV || 'development';
const configure = require('./config.js')[env];

const getStatus = function (req, res, next) {
    // make a request from ml server
    request('http://' + configure.ml_server_ip + ':' + configure.ml_server_port + '/status', function (reqe, response) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error connecting with machine leaning API" };
        }
        next();
    });

}

const getProgress = function (req, res, next) {
    // make a request from ml server
    request('http://' + configure.ml_server_ip + ':' + configure.ml_server_port + '/progress', function (reqe, response) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error communicating with machine leaning API" };
        }
        next();
    });
}

const detect = function (req, res, next) {
    var reqBody = JSON.stringify(req.body);

    const options = {
        url: 'http://' + configure.ml_server_ip + ':' + configure.ml_server_port + '/detect',
        method: 'POST',
        body: reqBody,
        headers: {
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'Connection': 'keep-alive',
            'json': true,
        }
    }
    request(options, function (reqe, response) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error communicating with machine leaning API" };
        }
        next();
    });
}

const startTraining = function( req, res, next) {

   var reqBody = req.body;
   reqBody.image_server = "http://" + configure.image_server_ip + ":" + configure.image_server_port + "/images/";
   
   const options = {
    url: 'http://' + configure.ml_server_ip + ':' + configure.ml_server_port + '/train',
    method: 'POST',
    body: JSON.stringify(reqBody),
    headers: {
        'Content-Type': 'application/json',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'json': true,
        }
   }

   request(options, function (reqe, response) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error communicating with machine leaning API" };
        }
        next();
   });
}



module.exports = { getStatus, getProgress, detect, startTraining };