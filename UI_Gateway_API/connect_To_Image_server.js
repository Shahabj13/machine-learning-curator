const request = require('request');
const dotenv = require('dotenv');
dotenv.config();
var env = process.env.NODE_ENV || 'development';
const configure = require('./config.js')[env];

// upload image to image server
const uploadImage = (req, res, next) => {
    const options = {
        url: 'http://' + configure.image_server_ip + ':' + configure.image_server_port + '/upload',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'Connection': 'keep-alive',
            'json': true,
        }
    }
    // send upload fequest to image server
    request(options, function (err, response, body) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error communicating with image server" };
        }
        next();
    });
}

const getImage = function (req, res, next){
    let filename = req.params.filename;

    request('http://' + configure.image_server_ip + ':' + configure.image_server_port + '/images/' + filename, function (reqe, response) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error communicating with image server" };
        }
        next();
    });
}


const getThumbnail = function (req, res, next){

    let filename = req.params.filename;

    request('http://' + configure.image_server_ip + ':' + configure.image_server_port + '/images/thumbnail/' + filename, async function (reqe, response) {
        try {
            req.body = JSON.parse(response.body);
        }
        catch (error){
            console.log(error);
            req.body = { response: "error", message: "network error communicating with image server" };
        }
        next();
    });
}


module.exports = { uploadImage, getImage, getThumbnail };
