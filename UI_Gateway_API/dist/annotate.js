// Node crypto library for creating filenames based on hashed image files
let crypto = require("crypto");

var img = undefined; // Image displayed on the canvas
var currentFilename = undefined; // Filename of image displayed on the canvas
var canvas = undefined;
var canvasElem = undefined;
var canvasMaxWidth = undefined;
var canvasMaxHeight = undefined;
var deleteButtonSVG = undefined;
var showButtonSVG = undefined;
var hideButtonSVG = undefined;
var isTyping = false;

// Database routes
const STORE_ANNOTATIONS_ROUTE = "/api/annotations";
const STORE_CLASS_ROUTE = "/api/classes";
const STORE_LABEL_ROUTE = "/api/labels";
const STORE_IMAGE_ROUTE = "/api/images";
const GET_ANNOTATIONS_ROUTE = "/api/annotations/";
const GET_CLASSES_ROUTE = "/api/classes";
const GET_LABELS_ROUTE = "/api/labels";
const GET_IMAGES_ROUTE = "/api/images";
const SEARCH_IMAGES_ROUTE = "/api/search/";
const DELETE_CLASS_ROUTE = "/api/classes/";
const DELETE_LABEL_ROUTE = "/api/labels/";
const DELETE_IMAGE_ROUTE = "/api/images/";

// Image server routes
const UPLOAD_IMAGE_ROUTE = "/image_server/upload";
const GET_THUMBNAIL_ROUTE = "/image_server/get_thumbnail/";
const GET_IMAGE_ROUTE = "/image_server/get_image/";

// Machine learning routes
const DETECTION_ROUTE = "/ml_connection/detect";
const TRAINING_ROUTE = "/ml_connection/train";
const PROGRESS_ROUTE = "/ml_connection/progress";

// Establish the canvas object when image is loaded
async function setupPage() {

	// Setup canvas
	const canvasContainer = document.getElementById('canvas-container');
	canvasElem = document.getElementById('canvas-draw');
	canvasMaxWidth = canvasContainer.width;
	canvasMaxHeight = canvasContainer.height;
	
	// React to page resizing
	const resizer = new ResizeObserver(element => {
		canvasMaxWidth = element[0].contentRect.width;
		canvasMaxHeight = element[0].contentRect.height;
		refreshCanvas();
	});
	resizer.observe(canvasContainer);

	// Prepare user interaction listeners
	canvasElem.addEventListener("mousemove", mouseMoveHandler, false);
	canvasElem.addEventListener("mousedown", mouseDownHandler, false);
	canvasElem.addEventListener("click", mouseClickHandler, false);
	canvasElem.addEventListener("mousewheel", mouseWheelHandler, { passive: false }); // Support Chromium
	canvasElem.addEventListener("DOMMouseScroll", mouseWheelHandler, false); // Support Firefox
	canvasElem.addEventListener("mouseout", mouseOutHandler, false);
	document.addEventListener("mouseup", mouseUpHandler, false);
	document.addEventListener("keyup", keyUpHandler, false);
	
	// Prepare HTML element SVGs
	setupSVGs();
	
	// Populate thumbnail side panel
	await populateSidePanel(true).then(async () => {
		// Populate classes and annotations
		await populateClassLabels().catch(error => {
			alert("Unable to populate labels and classes from database due to:\n" + error);
		});
	});

	// Search functionality listener
	document.getElementById('search-image-input').addEventListener('input', async event => {
		const input = event.target.value;
		await populateSidePanel(false, input);
	});

	// Prepare button listeners
	document.getElementById('reset-zoom-button').addEventListener('click', () => {
		if (currentFilename)
			canvas.resetZoom();
		else
			alert("Select or upload an image first!");
	});

	document.getElementById('clear-button').addEventListener('click', () => {
		if (currentFilename)
			canvas.clearPolygons();
		else
			alert("Select or upload an image first!");
	});

	document.getElementById('auto-annotate-button').addEventListener('click', () => {
		if (currentFilename)
			autoAnnotate();
		else
			alert("Select or upload an image first!");
	});

	document.getElementById('save-button').addEventListener('click', async () => {
		if (currentFilename)
			await saveAnnotations();
		else
			alert("Select or upload an image first!");
	});

	document.getElementById('training-button').addEventListener('click', async () => {
		await startTraining(false);
	});

	document.getElementById('tuning-button').addEventListener('click', async () => {
		await startTraining(true);
	});

	document.getElementById('status-button').addEventListener('click', async () => {
		await getTrainingStatus();
	});

	document.getElementById('delete-button').addEventListener('click', async () => {
		deleteImage();
	});

	document.getElementById('select-multiple-button').checked = false; // Uncheck select multiple button by default
	document.getElementById('select-multiple-button').addEventListener('change', (event) => {
		if (event.target.checked) {
			document.getElementById('select-multiple-label').classList.add("select-multiple-highlighted");
			resetHighlights(false);
		}
		else {
			document.getElementById('select-multiple-label').classList.remove("select-multiple-highlighted");
			
			// Remove existing highlighted thumbnails (aside from the one in the canvas)
			resetHighlights(true);
		}
	})

	// Create a new class/label when enter key is pressed in input field
	document.getElementById('create-class-label-input').addEventListener('keyup', (e) => {
		if (e.key === 'Enter') {
			const annotationInputField = document.getElementById('create-class-label-input');
			const annotationTypeRadio = document.getElementsByName('annotation-type-radio');
			const reg = /^[a-zA-Z ]+$/ // Only accept alphabetic values as a class/label name
			const validName = reg.test(annotationInputField.value);
			let success = undefined; // If the new class/label was added

			if (annotationInputField.value.length > 0 && validName && annotationInputField.value !== "undefined") {
				// Get input from create a class/label input field and convert to lowercase
				let annotationValue = annotationInputField.value.toLowerCase();

				// Check what annotation type the user has selected in the radio buttons
				let annotationType = null;
				for (let i = 0; i < annotationTypeRadio.length; ++i) {
					if (annotationTypeRadio[i].checked) {
						annotationType = annotationTypeRadio[i].value;
						break;
					}
				}

				// Append new class/label to respective dropdown box
				if (annotationType === 'class')
					addNewClass(annotationValue);

				else if (annotationType === 'label')
					addNewLabel(annotationValue);
			}
			else if (annotationInputField.value.length === 0) 
				alert("Name must have a length greater than 0.");

			else if (validName === false)
				alert("Name may only contain alphabetic characters.");
				
			else if (annotationInputField.value === "undefined")
				alert ("'undefined' is a reserved value");

				
			annotationInputField.value = ""; // Clear input field
		}
	});

	// User uploads new image to canvas
	document.getElementById('file-upload').addEventListener('change', async (e) => {
		let files = e.target.files;
		
		// Upload files to the image server and upload the filenames to the database
		await uploadFiles(files);
	});

	// Click listener to determine if user is typing in an input field
	window.addEventListener('click', (e) => {
		if (e.target.id === "search-image-input" || e.target.id === "create-class-label-input")
			isTyping = true;
		else
			isTyping = false;
	});

};

// Prepare SVG files for use in HTML buttons
function setupSVGs() {
	// Prepare SVG template
	let svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
	svg.setAttribute("width", "24");
	svg.setAttribute("height", "24");
	svg.setAttribute("viewBox", "0 0 24 24");
	svg.setAttribute("fill", "none");
	svg.setAttribute("stroke", "currentColor");
	svg.setAttribute("stroke-width", "2");
	svg.setAttribute("stroke-linecap", "round");
	svg.setAttribute("stroke-linejoin", "round");
	
	// Create hide button SVG
	hideButtonSVG = svg.cloneNode(true); // Clone element rather than referencing it
	let hideSVGPath = document.createElementNS("http://www.w3.org/2000/svg", 'path');
	hideSVGPath.setAttribute("d", "M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24");
	let hideSVGLine = document.createElementNS("http://www.w3.org/2000/svg", 'line');
	hideSVGLine.setAttribute("x1", "1");
	hideSVGLine.setAttribute("y1", "1");
	hideSVGLine.setAttribute("x2", "23");
	hideSVGLine.setAttribute("y2", "23");
	hideButtonSVG.appendChild(hideSVGPath);
	hideButtonSVG.appendChild(hideSVGLine);
	
	// Create show button SVG
	showButtonSVG = svg.cloneNode(true); // Clone element rather than referencing it
	let showSVGPath = document.createElementNS("http://www.w3.org/2000/svg", 'path');
	showSVGPath.setAttribute("d", "M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z");
	let showSVGCircle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
	showSVGCircle.setAttribute("cx", "12");
	showSVGCircle.setAttribute("cy", "12");
	showSVGCircle.setAttribute("r", "3");
	showButtonSVG.appendChild(showSVGPath);
	showButtonSVG.appendChild(showSVGCircle);
	
	// Create delete button SVG
	deleteButtonSVG = svg.cloneNode(true); // Clone element rather than referencing it
	let deleteSVGPolyline = document.createElementNS("http://www.w3.org/2000/svg", 'polyline');
	deleteSVGPolyline.setAttribute("points", "3 6 5 6 21 6");
	let deleteSVGPath = document.createElementNS("http://www.w3.org/2000/svg", 'path');
	deleteSVGPath.setAttribute("d", "M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2");
	let deleteSVGLine1 = document.createElementNS("http://www.w3.org/2000/svg", 'line');
	deleteSVGLine1.setAttribute("x1", "10");
	deleteSVGLine1.setAttribute("y1", "11");
	deleteSVGLine1.setAttribute("x2", "10");
	deleteSVGLine1.setAttribute("y2", "17");
	let deleteSVGLine2 = document.createElementNS("http://www.w3.org/2000/svg", 'line');
	deleteSVGLine2.setAttribute("x1", "14");
	deleteSVGLine2.setAttribute("y1", "11");
	deleteSVGLine2.setAttribute("x2", "14");
	deleteSVGLine2.setAttribute("y2", "17");
	deleteButtonSVG.appendChild(deleteSVGPolyline);
	deleteButtonSVG.appendChild(deleteSVGPath);
	deleteButtonSVG.appendChild(deleteSVGLine1);
	deleteButtonSVG.appendChild(deleteSVGLine2);
}

// Reset image object, and determine if it should be uploaded to image server
async function resetImage() {
	img = new Image();
	// Wait until image is loaded into an Image()
	img.onload = async function () {
		refreshCanvas().then(() => {
			if (img.annotations) {
				// Transform coordinates according to new canvas dimensions
				let canvasWidth = canvas.getWidth();
				let canvasHeight = canvas.getHeight();
				for (let i = 0; i < img.annotations.length; ++i) {
					for (let j = 0; j < img.annotations[i].points.length; ++j) {
						img.annotations[i].points[j][0] *= canvasWidth;
						img.annotations[i].points[j][1] *= canvasHeight;
					}
				}
				canvas.addAnnotations(img.annotations);
			}
		});
	}
}

// Change the image currently displayed on the canvas, and upload it to the image server if needed
async function refreshCanvas() {
	resetCanvas();
	canvas.ctx = canvasElem.getContext('2d');
	canvas.ctx.canvas.height = canvasMaxHeight;
	canvas.ctx.canvas.width = canvasMaxWidth;
	canvas.drawImage();
	updateAnnotations();
}

// Add classes and labels from database to UI
async function populateClassLabels() {
	let classesJSON = await getRequest(GET_CLASSES_ROUTE)
		.catch(error => {
			throw error;
		});
	
	let classes = [];
	for (let i = 0; i < classesJSON.results.length; ++i) {
		classes.push(classesJSON.results[i].class_name);
	}
	refreshClassLabelDropdown(classes, "classes");

	let labelsJSON = await getRequest(GET_LABELS_ROUTE)
		.catch(error => {
			throw error;
		});

	let labels = [];
	for (let i = 0; i < labelsJSON.results.length; ++i) {
		labels.push(labelsJSON.results[i].label_name);
	}
	refreshClassLabelDropdown(labels, "labels");
}

// Populate the thumbnail side panel with the thumbnails from the image server
async function populateSidePanel(setup, searchTerm) {
	
	let filenamesJSON = undefined;
	let searching = (searchTerm) ? true:false;

	// Get all images filenames that are relevant to the search term
	if (searchTerm)
		filenamesJSON = await getRequest(SEARCH_IMAGES_ROUTE + searchTerm)
				.catch(error => {
					alert("Unable to get images from database due to:\n" + error);
				});

	// Get all image filenames in database
	else
		filenamesJSON = await getRequest(GET_IMAGES_ROUTE)
				.catch(error => {
					alert("Unable to get images from database due to:\n" + error);
				});
	if (!filenamesJSON)
		return;

	// Wipe the existing gallery
	let thumbnailGallery = document.getElementById("image-gallery");
	thumbnailGallery.innerHTML = "";
	
	if (!filenamesJSON || filenamesJSON.response !== "success") {
		alert("Error loading image information from the database!");
		thumbnailGallery.innerHTML = "<b>Error loading image information from the database.<b>";
		return;
	}
	
	let filenames = (searching) ? filenamesJSON.results : filenamesJSON.filenames;
	if (filenames.length === 0) {
		thumbnailGallery.innerHTML = "<b>Uploaded images will show here.<b>";
		return;
	}
	
	// Create HTML element templates
	let rowTemplate = document.createElement('div');
	rowTemplate.classList.add("row", "h-40", "thumbnail-row", "d-flex", "justify-content-center");
	let columnTemplate = document.createElement('div');
	columnTemplate.classList.add("col-md-5", "thumbnail");
	let radioTemplate = document.createElement("input");
	radioTemplate.classList.add("thumbnail-radio");
	radioTemplate.setAttribute("type", "radio");
	radioTemplate.setAttribute("name", "thumbnail-radio");
	let labelTemplate = document.createElement("label");
	labelTemplate.classList.add("thumbnail-label");
	
	// Create HTML elements
	let currentRow = rowTemplate.cloneNode(true);
	for (let i = filenames.length-1; i >= 0; --i) { // Iterate in reverse to show most recent images at top

		const ascending = filenames.length - i - 1; // Ascending count of thumbnails

		const filename = (searching) ? filenames[i] : filenames[i].image_filename;
	
		// Create a new column, radio, and label
		let column = columnTemplate.cloneNode(true);
		if (ascending % 2 == 1)
			column.classList.add("right-thumbnail");
		else
			column.classList.add("left-thumbnail");
		if (ascending < 2)
			column.classList.add("top-thumbnail");
		let radio = radioTemplate.cloneNode(true);
		let label = labelTemplate.cloneNode(true);
		
		// Set attributes
		let id = "thumbnail-" + (ascending + 1).toString();
		radio.id = id;
		radio.setAttribute("data-filename", filename);
		label.setAttribute("for", id);
		
		let image = document.createElement('img');
		image.classList.add("thumbnail-image");
		image.src = "../images/thumbnail-placeholder.jpg";
		image.setAttribute("data-filename", filename);

		label.setAttribute("data-filename", filename);
		if (!setup && filename === currentFilename)
			label.classList.add("highlighted-thumbnail")
		
		label.appendChild(image);
		column.appendChild(radio);
		column.appendChild(label);
		currentRow.appendChild(column);
		
		// Add the current row and make a new row
		if (ascending % 2 === 1 || ascending === filenames.length - 1) {
			thumbnailGallery.appendChild(currentRow);
			currentRow = rowTemplate.cloneNode(true);
		}
	}

	// Add the first image in the side panel to the canvas on login/reload
	let firstThumbnail = document.getElementById("thumbnail-1");
	if (setup && firstThumbnail) {
		await getImage(firstThumbnail.getAttribute("data-filename"))
			.then(res => {
				currentFilename = firstThumbnail.getAttribute("data-filename");
				let radioLabel = document.querySelector('label[for=' + firstThumbnail.id + ']');
				radioLabel.classList.add("highlighted-thumbnail");
			})
			.catch(error => {
				alert("Unable to load image from sidepanel to annotation screen.");
			});
	}
	
	const observer = new IntersectionObserver(async function(thumbnails, self) {
		thumbnails.forEach(async thumbnail => {
			if(thumbnail.isIntersecting) {
				let filename = thumbnail.target.getAttribute("data-filename");
			
				await getRequest(GET_THUMBNAIL_ROUTE + filename)
					.then(res => {
						thumbnailb64 = res.image;
						thumbnailb64 = "data:image/" + filename.split('.').pop() + ";base64," + thumbnailb64; // Add MIME to b64
						thumbnail.target.src = thumbnailb64;
					})
					.catch(error => {
						thumbnail.target.src = "../images/thumbnail-error-placeholder.jpg";
					});
				observer.unobserve(thumbnail.target);
			}
		});
	});
	
	// Establish lazy loading
	document.querySelectorAll(".thumbnail-image").forEach(image => { observer.observe(image) });

	// Setup thumbnail listeners
	await setupThumbnailListeners();
}

async function setupThumbnailListeners() {
	document.querySelectorAll('.thumbnail-radio').forEach(async item => {
		item.addEventListener('click', async event => {
			// If selecting an image to display on the canvas
			if (!document.getElementById("select-multiple-button").checked) {
				if (canvas.unsavedChanges) {
					if (!confirm("The current image has unsaved changes, are you sure you want to continue?"))
						return;
				}
				let filename = item.getAttribute("data-filename")
				await getImage(filename)
					.then(res => {
						currentFilename = item.getAttribute("data-filename");

						// Remove currently highlighted thumbnail and add the new one
						resetHighlights(true);
	
					})
					.catch(error => {
						alert("Failed to fetch image and annotations due to:\n" + error);
					});
			}
			// If selecting images to use for retraining/deletion
			else {
				let radioLabel = document.querySelector('label[for=' + item.id + ']');

				// If already highlighted, remove the highlight, otherwise add it
				if (radioLabel.classList.contains("highlighted-thumbnail"))
					radioLabel.classList.remove("highlighted-thumbnail");
				else {
					radioLabel.classList.add("highlighted-thumbnail");

					/* If the user is holding shift, select all thumbnails between the clicked one and
					   the previous highlighted one (or the beginning) */
					if (event.shiftKey) {
						let numStart = item.id.indexOf("-");
						let num = parseInt(item.id.substring(numStart+1));
						
						// Find the number of the preceeding highlighted thumbnail (or 0)
						let start = 0;
						for (let i = num-1; i > 0; --i) {
							if (document.querySelector('label[for=thumbnail-' + i + ']').classList.contains("highlighted-thumbnail")) {
								start = i;
								break;
							}
						}

						// Highlight all thumbnails between the start and clicked thumbnail
						for (let i = start + 1; i < num; ++i) {
							document.querySelector('label[for=thumbnail-' + i + ']').classList.add("highlighted-thumbnail");
						}
					}
				}
			}
		});
	});
}

// Remove highlighted thumbnails and highlight the current thumbnail
function resetHighlights(highlightCanvas) {
	let oldHighlights = document.getElementsByClassName("highlighted-thumbnail");
	if (oldHighlights.length) {
		for (let i = 0; i < oldHighlights.length; ++i) {
			oldHighlights[i].classList.remove("highlighted-thumbnail");
			i -= 1;
		}
	}

	if (highlightCanvas) {
		let radioLabel = document.querySelector("label[data-filename='" + currentFilename + "']");
		radioLabel.classList.add("highlighted-thumbnail");
	}
}

// Go to the previous or image in the sidepanel
function changeImage(step) {
	// Prevent changing the image if selecting multiple thumbnails
	if (document.getElementById("select-multiple-button").checked) {
		alert("Can't change image while selecting thumbnails.");
		return;
	}

	let oldHighlights = document.getElementsByClassName("highlighted-thumbnail");
	if (oldHighlights.length) {
		let currentHighlight = oldHighlights[0];
		let numStr = currentHighlight.getAttribute("for");
		let numStart = numStr.indexOf("-");
		let num = parseInt(numStr.substring(numStart+1));

		// Next image
		if (step > 0) {
			num += 1;
			let nextThumbnail = document.getElementById("thumbnail-" + num.toString());
			if (nextThumbnail) {
				let filename = nextThumbnail.getAttribute("data-filename");
				currentFilename = filename;
				getImage(filename);
				resetHighlights(true);
			}
		}

		// Previous image
		else {
			if (num !== 1) {
				num -= 1;
				let filename = document.getElementById("thumbnail-" + num.toString()).getAttribute("data-filename");
				currentFilename = filename;
				getImage(filename);
				resetHighlights(true);
			}
		}
	}

}

// Delete an image along with its associated annotations
async function deleteImage() {
	let highlightedThumbnails = document.getElementsByClassName("highlighted-thumbnail");
	if (!highlightedThumbnails.length) {
		alert("No images selected!");
		return;
	}

	if (!confirm("This will delete all selected images in the sidepanel, along with their annotations. These cannot be recovered. Do you want to continue?"))
		return;

	let needsNewImage = false; // Whether the canvas image is deleted and needs a new image from the sidepanel
		
	// If selecting multiple images for deletion
	try {
		for (let i = 0; i < highlightedThumbnails.length; ++i) {
			const filename = highlightedThumbnails[i].getAttribute("data-filename");
			if (filename === currentFilename) {
				needsNewImage = true;
				resetImage();
				refreshCanvas();
				canvas.ctx.canvas.width = canvasMaxWidth;
				canvas.ctx.canvas.height = canvasMaxHeight;
				currentFilename = undefined;
			}
			await deleteRequest(DELETE_IMAGE_ROUTE + filename)
				.catch((error) => {
					throw error;
				});
		}
	}
	catch (error) {
		alert("Unable to delete images from database due to:\n" + error);
		return;
	}

	// Exit multiple image selection mode (if in it)
	if (document.getElementById("select-multiple-button").checked) {
		document.getElementById("select-multiple-button").checked = false;
		document.getElementById('select-multiple-label').classList.remove("select-multiple-highlighted");
	}

	await populateSidePanel(needsNewImage);
}

// Download an image from the image server and its annotations from the database, and add it to the canvas
async function getImage(filename) {
	
	// Get image from image server
	getRequest(GET_IMAGE_ROUTE + filename)
		.then(async imageJSON => {	

			// Get annotations for image from database
			getRequest(GET_ANNOTATIONS_ROUTE + filename)
				.then(async annotationsJSON => {
					imageb64 = imageJSON.image;
					imageb64 = "data:image/" + filename.split('.').pop() + ";base64," + imageb64; // Add MIME to b64
					resetImage().then(() => {
						let annotations = [];
						for (let i = 0; i < annotationsJSON.results.length; ++i) {
							annotations.push({ID: i+1, class: "", label: "", points: [] });
							annotations[i].class = annotationsJSON.results[i].class_name;
							annotations[i].label = annotationsJSON.results[i].label_name;
							annotations[i].points = annotationsJSON.results[i].points;
						}
						if (annotations.length)
							img.annotations = annotations;
						else
							img.annotations = undefined;
						img.src = imageb64;
					});
				})		
				.catch(error => {
					throw error;
				})

		})
		.catch(error => {
			alert("Could not retrieve image from image server due to:\n" + error);
		});	
}

// Get a base64 encode of a given file
async function getBase64(file) {
	return new Promise((resolve) => {
	    	const reader = new FileReader();
	    	reader.readAsDataURL(file);
	    	reader.onload = () => resolve(reader.result);
  	});
}

// Upload a given list of files to the image server and database
async function uploadFiles(files) {
	
	let counter = 0; // Counter to keep track of how many files are uploaded
	let successfulUpload = false; // Whether any images have been uploaded
	let filesNum = files.length;
	
	// Make sure filename lengths are under 71 chars
	for (let i = 0; i < filesNum; ++i) {
		if (files[i].name.length > 70) {
			alert("Please upload files with no greater than 70 characters in the filename.");
			document.getElementById('file-upload').value = "";
			return;
		}
	}

	// Iterate through list of uploaded files and upload them to the image server and UI
	for (let i = 0; i < filesNum; ++i) {
		// Get a base64 encode of the image
		getBase64(files[i]).then(async image => {
			// Cut metadata from start of base64 string
			let b64Start = image.indexOf('base64');
			let rawb64 = image.substring(b64Start + 7);
			
			let oldImageName = files[i].name.toLowerCase();
			let imageExtension = oldImageName.split('.').pop();
			
			// Generate SHA-256 hash to use for the unique filename
			const hash = crypto.createHash('sha256').update(rawb64).digest('hex');
			let newImageName = hash + "." + imageExtension;
			
			// Prepare JSON payload for upload to image server
			const imageToUploadJSON = JSON.stringify({
				filename: newImageName,
				image: rawb64
			});
			
			
			postRequest(imageToUploadJSON, UPLOAD_IMAGE_ROUTE)
				.then(async res => {
					const reqBody = JSON.stringify({image_filename: newImageName, original_filename: oldImageName});
					postRequest(reqBody, STORE_IMAGE_ROUTE)
						.then(async () => {
							++counter;
							successfulUpload = true;
							await finishUploading(counter, filesNum, successfulUpload);
						})
						.catch(async error => {
							alert("Failed to store " + oldImageName + " image information in database due to:\n" + dbError);
							++counter;
							await finishUploading(counter, filesNum, successfulUpload);
						});
				})
				.catch(async error => {
					/* If image already exists in the image server, try storing it in the database.
						If it also exists in the database, display the relevant error */
					if (error === "file already exists") {
						const reqBody = JSON.stringify({image_filename: newImageName, original_filename: oldImageName});
						postRequest(reqBody, STORE_IMAGE_ROUTE)
							.then(async res => {
								++counter;
								successfulUpload = true;
								await finishUploading(counter, filesNum, successfulUpload);
							})
							.catch(async err => {
								alert("Image could not be referenced in the database due to:\n" + err);
								++counter;
								await finishUploading(counter, filesNum, successfulUpload);
							});
					}
					else {
						++counter;
						await finishUploading(counter, filesNum, successfulUpload);
						alert("Failed to upload " + oldImageName + " to image server due to:\n" + error);
					}
				});
		});
	}
}

// Populate sidepanel and clear the upload button if uploading is finished
async function finishUploading(counter, filesNum, successfulUpload) {
	if (counter === filesNum) {
		document.getElementById('file-upload').value = "";
		populateSidePanel(false).then(async () => {
			if (successfulUpload) {
				let latestThumbnail = document.getElementById("thumbnail-1");
				if (latestThumbnail) {
					let filename = latestThumbnail.getAttribute("data-filename");
					if (canvas.unsavedChanges) {
						if (confirm("You have unsaved changes. Are you sure you want to change images?")) {
							currentFilename = filename;
							resetHighlights(true);
							await getImage(filename);
						}
					}
					else {
						currentFilename = filename;
						resetHighlights(true);
						await getImage(filename);
					}	
				}
			}
		});
	}
}

// Update the classes in the manage classes dropdown
function updateClassLabels() {
	classTable = document.getElementById('manage-classes').getElementsByTagName('tbody')[0];
	labelTable = document.getElementById('manage-labels').getElementsByTagName('tbody')[0];

	allClasses = getAllClasses();
	allLabels = getAllLabels();
	
	updateTable(classTable, allClasses, "class");
	updateTable(labelTable, allLabels, "label");
	
	createClassDeleteListeners();
	createLabelDeleteListeners();
}

function updateTable(table, values, type) {
	// Delete existing rows
	table.innerHTML = "";
	
	// Add rows
	for (let i = 0; i < values.length; ++i) {
		let row = table.insertRow();
		let firstCell = row.insertCell(0);
		let deleteButtonCell = row.insertCell(1);
		
		firstCell.innerHTML = values[i].value;
		
		let deleteButton = document.createElement('button');
		deleteButton.appendChild(deleteButtonSVG.cloneNode(true));
		deleteButton.classList.add("delete-" + type + "-button", "btn", "btn-primary");
		deleteButton.setAttribute("data-" + type, values[i].value);
		deleteButton.setAttribute("title", "Delete this value from the available classes or labels.")
		deleteButtonCell.appendChild(deleteButton);
	}
}

// Create listeners for delete class buttons in the right-hand side panel
function createClassDeleteListeners() {
	document.querySelectorAll('.delete-class-button').forEach(item => {
		item.addEventListener('click', event => {
			deleteClass(item.getAttribute("data-class"));
		});
	});
}

// Create listeners for delete class buttons in the right-hand side panel
function createLabelDeleteListeners() {
	document.querySelectorAll('.delete-label-button').forEach(item => {
		item.addEventListener('click', event => {
			deleteLabel(item.getAttribute("data-label"));
		});
	});
}

// Delete a class if it is not in use
function deleteClass(className) {
	allClasses = canvas.getClasses();
	if (allClasses.includes(className)) {
		alert("Class is in use by an annotation.");
		return;
	}

	deleteRequest(DELETE_CLASS_ROUTE + className)
		.then(() => {
			let classDropdown = document.getElementById('selectable-classes');
			let values = classDropdown.options;
			for (let i = 0; i < values.length; ++i) {
				if (values[i].value === className) {
					classDropdown.remove(i);
					updateClassLabels();
					canvas.updateAnnotations();
					return;
				}	
			}
		})
		.catch(error => {
			alert("Unable to delete class from database due to:\n" + error);
		});
}

// Delete a label if it is not in use
function deleteLabel(labelName) {
	allLabels = canvas.getLabels();
	if (allLabels.includes(labelName)) {
		alert("Label is in use by an annotation.");
		return;
	}

	deleteRequest(DELETE_LABEL_ROUTE + labelName)
		.then(() => {
			let labelDropdown = document.getElementById('selectable-labels');
			let values = labelDropdown.options;
			for (let i = 0; i < values.length; ++i) {
				if (values[i].value === labelName) {
					labelDropdown.remove(i);
					updateClassLabels();
					canvas.updateAnnotations();
					return;
				}
			}
		})
		.catch(error => {
			alert("Unable to delete label from database due to:\n" + error);
		});
}


// Update the annotations list on the right-hand sidepanel
function updateAnnotations(polygons, hiddenPolygons) {

	// Clear existing table rows
	document.getElementById('manage-annotations').getElementsByTagName('tbody')[0].innerHTML = '';
	
	let totalPolygons = canvas.getPolygonsLength();
	
	// Iterate through total number of annotations if any exist
	for (let i = 0; i < totalPolygons; ++i) {
		for (let j = 0; j < totalPolygons; ++j) {
			if (polygons[j] && polygons[j].ID === i + 1) {
				appendAnnotationRow(i + 1, polygons[j].class, polygons[j].label, false);
				break;
			}
			else if (hiddenPolygons[j] && hiddenPolygons[j].ID === i + 1) {
				appendAnnotationRow(i + 1, hiddenPolygons[j].class, hiddenPolygons[j].label, true);
				break;
			}
		}
	}
	
	createShowHideListeners();
	createAnnotationDeleteListeners();
	createClassListeners();
	createLabelListeners();
}

// Appends a row to the bottom of the annotations list table
function appendAnnotationRow(id, className, labelName, hidden) {
	let labelsTable = document.getElementById('manage-annotations').getElementsByTagName('tbody')[0];
	let row = labelsTable.insertRow();
	let idCell = row.insertCell(0);
	let classCell = row.insertCell(1);
	let labelCell = row.insertCell(2);
	let visibilityButtonCell = row.insertCell(3);
	let deleteButtonCell = row.insertCell(4);

	idCell.innerHTML = id;

	let classDropdown = prepareAnnotateDropdown(className, getAllClasses());
	classDropdown.classList.add("class-dropdown", "form-select");
	classDropdown.setAttribute("data-annotation-id", id);
	classCell.appendChild(classDropdown);
	
	let labelDropdown = prepareAnnotateDropdown(labelName, getAllLabels());
	labelDropdown.classList.add("label-dropdown", "form-select");
	labelDropdown.setAttribute("data-annotation-id", id);
	labelCell.appendChild(labelDropdown);

	// Change the button icon depending on if the annotation is visible
	let showHideSVG = undefined;
	if (hidden) {
		showHideSVG = showButtonSVG.cloneNode(true);
		row.classList.add("table-active"); // Make row darker if annotation isn't visible
	}
	else
		showHideSVG = hideButtonSVG.cloneNode(true);
	
	// Setup show/hide button
	let showHideButton = document.createElement('button');
	showHideButton.classList.add("show-hide-button", "btn", "btn-primary");
	showHideButton.setAttribute("data-annotation-id", id);
	showHideButton.setAttribute("data-hidden", hidden.toString());
	showHideButton.setAttribute("title", "Show or hide this annotation on the image.")
	showHideButton.appendChild(showHideSVG);
	visibilityButtonCell.appendChild(showHideButton);
	
	// Setup delete button
	let deleteButton = document.createElement('button');
	deleteButton.classList.add("delete-annotation-button", "btn", "btn-primary");
	deleteButton.appendChild(deleteButtonSVG.cloneNode(true));
	deleteButton.setAttribute("data-annotation-id", id);
	deleteButton.setAttribute("title", "Delete this annotation.")
	deleteButtonCell.appendChild(deleteButton);
}

// Prepare a dropdown list for the right-hand side panel
function prepareAnnotateDropdown(selected, values) {
	let dropdown = document.createElement("select");
	if (selected === "undefined") {
		undefinedOption = document.createElement("option");
		undefinedOption.value = "undefined";
		undefinedOption.text = "undefined";
		undefinedOption.setAttribute("selected", "selected");
		dropdown.appendChild(undefinedOption);
	}
	
	for (let i = 0; i < values.length; ++i) {
		let option = document.createElement("option");
		option.value = values[i].value;
		option.text = values[i].value;
		
		// Make the default selection if it is equal to the polygon's class
		if (values[i].value === selected)
			option.setAttribute("selected", "selected");
		
		dropdown.appendChild(option);
	}
		
	return dropdown;
}

// Create listeners for show/hide buttons in the right-hand side panel
function createShowHideListeners() {
	document.querySelectorAll('.show-hide-button').forEach(item => {
		item.addEventListener('click', event => {
			let type = item.getAttribute("data-hidden");
			let ID = parseInt(item.getAttribute("data-annotation-id"));
			
			if (type === "false")
				canvas.hidePolygon(ID);
			else
				canvas.showPolygon(ID);

		});
	});
}

// Create listeners for delete annotation buttons in the right-hand side panel
function createAnnotationDeleteListeners() {
	document.querySelectorAll('.delete-annotation-button').forEach(item => {
		item.addEventListener('click', event => {
			canvas.deletePolygon(parseInt(item.getAttribute("data-annotation-id")));
		});
	});
}

// Create listeners for class dropdowns in the right-hand side panel
function createClassListeners() {
	document.querySelectorAll('.class-dropdown').forEach(item => {
		item.addEventListener('change', event => {
			let ID = parseInt(item.getAttribute("data-annotation-id"));
			let newValue = event.target.value;
			canvas.changeClassLabel(ID, "class", newValue);
		});
	});
}

// Create listeners for label dropdowns in the right-hand side panel
function createLabelListeners() {
	document.querySelectorAll('.label-dropdown').forEach(item => {
		item.addEventListener('change', event => {
			let ID = parseInt(item.getAttribute("data-annotation-id"));
			let newValue = event.target.value;
			canvas.changeClassLabel(ID, "label", newValue);
		});
	});
}

// Retrieve auto annotations from ML API
async function autoAnnotate() {

	// Create temporary HTML canvas with image and render it into a compressed JPEG as a base64 string
	var tempCanvas = document.createElement('canvas');
    tempCanvas.height = img.naturalHeight;
    tempCanvas.width = img.naturalWidth;
    var tempCtx = tempCanvas.getContext('2d');
    tempCtx.drawImage(img, 0, 0, tempCanvas.width, tempCanvas.height);
	base64Image = tempCanvas.toDataURL("image/jpeg", 0.6);
	
	// Cut MIME metadata from start of base64 encode
	var strStart = base64Image.indexOf('base64');
	var base64Image = base64Image.substring(strStart + 7);

	// Create JSON data payload to send to ML API
	const reqBody = JSON.stringify({ image: base64Image });
	
	// Upload image to ML API to retrieve detections
	postRequest(reqBody, DETECTION_ROUTE)
		.then(res => {
			if (res.response === "success")
				transformAnnotations(res);
			else
				alert("No objects could be detected.");
		})
		.catch(error => {
			if (error === "no objects detected")
				alert("No objects detected in image.");
			else
				alert("Unable to communicate with machine learning API due to:\n" + error);
		});
}

// Get the training progress of the machine learning model
async function getTrainingStatus() {
	getRequest(PROGRESS_ROUTE)
		.then(res => {
			alert("The training status of the machine learning model is:\n" + res.progress);
		})
		.catch(error => {
			alert("Unable to get training status from machine learning API due to:\n" + error);
		});
}

// Start training or fine-tuning the machine learning model
async function startTraining(finetune) {
	let highlightedThumbnails = document.getElementsByClassName("highlighted-thumbnail");
	if (!highlightedThumbnails.length) {
		alert("No images selected!");
		return;
	}
	
	let progress = undefined;
	try {
		progress = await getRequest(PROGRESS_ROUTE)
			.catch(error => {
				alert("Unable to get training status from machine learning API due to:\n" + error);
				throw error;
			});
	}
	catch {
		return;
	}
	
	if (!progress)
		return;

	if (progress.progress !== "stopped") {
		alert("Machine learning model is already training. It's current progress is:\n" + progress.progress);
		return;
	}

	// Create the JSON request with image filenames and annotations to send to the machine learning model
	let reqBody = { image_server: "", epochs: 30, finetune: finetune, images: [] };
	let counter = 0;
	
	for (let i = 0; i < highlightedThumbnails.length; ++i) {
		const filename = highlightedThumbnails[i].getAttribute("data-filename");

		// Get annotations from database for current highlighted thumbnail
		getRequest(GET_ANNOTATIONS_ROUTE + filename)
				.then(annotationsJSON => {
					let newImage = { filename: filename, annotations: [] };
					for (let j = 0; j < annotationsJSON.results.length; ++j) {
						let highest = annotationsJSON.results[j].points[0][1];
						let lowest = annotationsJSON.results[j].points[0][1];
						let mostLeft = annotationsJSON.results[j].points[0][0];
						let mostRight = annotationsJSON.results[j].points[0][0];
						for (let k = 0; k < annotationsJSON.results[j].points.length; ++k) {
							const xPoint = annotationsJSON.results[j].points[k][0];
							const yPoint = annotationsJSON.results[j].points[k][1];
							if (yPoint > highest)
								highest = yPoint;
							if (yPoint < lowest)
								lowest = yPoint;
							if (xPoint < mostLeft)
								mostLeft = xPoint;
							if (xPoint > mostRight)
								mostRight = xPoint;
						}
						const w = mostRight - mostLeft;
						const h = highest - lowest;
						const x = mostLeft + (w / 2);
						const y = lowest + (h / 2);
						const coordinates = x.toString() + " " + y.toString() + " " + w.toString() + " " + h.toString();
						newImage.annotations.push({ label: annotationsJSON.results[j].label_name, bounding_box: coordinates });
					}

					// If annotations exist in the image, add it to the request body
					if (newImage.annotations.length)
						reqBody.images.push(newImage);

					counter++;

					if (counter === highlightedThumbnails.length) {
						if (!reqBody.images.length) {
							alert("No annotations are present in the selected images!");
							throw error;
						}
						try {
							reqBody.epochs = parseInt(prompt("Enter the number of epochs to train for (cycles of training data):", "100"));
							if (reqBody.epochs === undefined)
								return;
							if (isNaN(reqBody.epochs)) // If not a number
								throw "Not a number";
							else if (reqBody.epochs < 1)
								throw "Below one";
						}
						catch (error) {
							alert("Epochs must be a number (1 or more)!");
							throw error;
						}
						// Send for training
						postRequest(JSON.stringify(reqBody), TRAINING_ROUTE)
							.then(() => {
								alert("Training has started. View its progress with the 'status' button.");
							})
							.catch(error => {
								alert("Unable to start training due to:\n" + error);
							});
					}
				}).catch(() => {
					return;
				});
	}
}

// Upload classes, labels, and annotations to the database
async function saveAnnotations() {
	if (this.mode === canvasMode.cDrawPoly) {
		this.finishPoly();
	}

	// Upload new classes and labels
	uploadClassLabels().then(async () => {
		
		// Normalise coordinates and upload annotations
		var original = canvas.getPolygons();
		var normalised = [];
		
	
		for (let i = 0; i < original.length; ++i) {
			normalised.push({ class: original[i].class, label: original[i].label, points: [] });
			for (let j = 0; j < original[i].points.length; ++j) {
				normalised[i].points.push([]);
				normalised[i].points[j].push(normaliseWidth(original[i].points[j][0]));
				normalised[i].points[j].push(normaliseHeight(original[i].points[j][1]));
			}
		}

		let reqBody = JSON.stringify({ image_filename: currentFilename, annotations: normalised });
		postRequest(reqBody, STORE_ANNOTATIONS_ROUTE).then(async () => {
			canvas.unsavedChanges = false;

			// Make save button flash white and say "saved" for 2 seconds
			document.getElementById("save-label").classList.add("saved-label");
			document.getElementById("save-label").innerHTML = "Saved!";
			await new Promise(res => setTimeout(res, 1000));
			document.getElementById("save-label").classList.remove("saved-label");
			document.getElementById("save-label").innerHTML = "Save";
		})
		.catch(error => {
			alert("Unable to upload annotations to database due to:\n" + error);
		})

	})
	.catch(error => {
		alert("Unable to upload classes and labels to database due to:\n" + error);
	});
}

// Upload any new classes and labels to the database
async function uploadClassLabels() {
	
	// Retrieve existing labels and classes
	let existingClasses = [];
	try {
		let classesJSON = await getRequest(GET_CLASSES_ROUTE)
			.catch(error => {
				throw error;
			});
		for (let i = 0; i < classesJSON.results.length; ++i) {
			existingClasses.push(classesJSON.results[i].class_name);
		}
	}
	catch (error){
		throw error;
	}

	let existingLabels = [];
	try {
		let labelsJSON = await getRequest(GET_LABELS_ROUTE)
			.catch(error => {
				throw error;
			});
		for (let i = 0; i < labelsJSON.results.length; ++i) {
			existingLabels.push(labelsJSON.results[i].label_name);
		}
	}
	catch (error){
		throw error;
	}

	// Find any new classes or labels to upload
	let newClasses = getAllClasses();
	let newLabels = getAllLabels();
	
	let classesToUpload = [];
	for (let i = 0; i < newClasses.length; ++i) {
		if (!existingClasses.includes(newClasses[i].value)) {
			classesToUpload.push(newClasses[i].value);
		}
	}

	let labelsToUpload = [];
	for (let i = 0; i < newLabels.length; ++i) {
		if (!existingLabels.includes(newLabels[i].value)) {
			labelsToUpload.push(newLabels[i].value);
		}
	}


	// Upload new classes and labels
	for (let i = 0; i < classesToUpload.length; ++i) {
		const reqBody = JSON.stringify({class_name: classesToUpload[i]})
		await postRequest(reqBody, STORE_CLASS_ROUTE)
			.catch(error => {
				throw error;
			});
	}
	for (let i = 0; i < labelsToUpload.length; ++i) {
		const reqBody = JSON.stringify({label_name: labelsToUpload[i]})
		await postRequest(reqBody, STORE_LABEL_ROUTE)
			.catch(error => {
				throw error;
			});
	}
}

// Send an HTTP POST to a given route with a given JSON body
async function postRequest(reqBody, route) {
	let postRequestOpts = {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: reqBody,
		json: true
	}

	try {
		let postResponse = await fetch(route, postRequestOpts);
		postResponse = await postResponse.json();

		if (postResponse.response !== "success") {
			throw postResponse.message;
		}
		return postResponse;
	}
	catch (error) {
		throw error;
	}
}

// Send an HTTP GET to a given route
async function getRequest(route) {
	const getRequestOpts = {
		method: 'GET',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}

	try {
		let getResponse = await fetch(route, getRequestOpts);
		getResponse = await getResponse.json();

		if (getResponse.response) {
			if (getResponse.response !== "success") {
				throw getResponse.message;
			}
		}
		return getResponse;
	}
	catch (error) {
		throw error;
	}
}

// Send an HTTP DELETE to a given route
async function deleteRequest(route) {
	const deleteRequestOpts = {
		method: 'DELETE',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}

	try {
		let getResponse = await fetch(route, deleteRequestOpts);
		getResponse = await getResponse.json();

		if (getResponse.response !== "success") {
			throw getResponse.message;
		}
		return getResponse;
	}
	catch (error) {
		throw error;
	}
}

// Status of the canvas
var canvasMode = Object.freeze({
	cNone: "none",
	cDragPt: "drag",
	cSelPt: "select",
	cDrawBox: "drawBox",
	cDrawPoly: "drawPoly",
	cdragImage: "dragImage"
});

// Canvas object
function resetCanvas() {
	canvas = {
		hiddenPolygons: [],	// Non-visible
		polygons: [],		// Visible polygons
		hoverPoly: undefined, 	// polygons[] index of polygon mouse is hovering over
		hoverPt: undefined, 	// polygons[hoverPoly] index of point mouse is hovering over
		selPoly: undefined, 	// polygons[] index of polygon mouse has clicked on
		selPt: undefined, 	// polygons[selPoly].points index of point mouse has clicked on
		ctx: undefined, 	// 2D context of canvas element
		zoom: 1, 		// The amount of zoom currently used in the canvas
		offsetX: 0,		// The x and y offset from the original image position
		offsetY: 0,
		startDragX: 0,
		startDragY: 0,
		centerX: 0,		// The x and y offsets adjusted to the zoom level for drawing the canvas
		centerY: 0,
		zoomLimit: false,
		imgRatio: undefined, 	// The value required to adjust the image to fit the canvas
		unsavedChanges: false, // Whether unsaved annotations exist in the image
		mode: canvasMode.cNone,

		// Adjust the canvas according to the zoom and offset, and draw the image and annotations
		drawCanvas: function (offsetX, offsetY) {
			// Set focus point for painting the new canvas
			this.centerX = ((this.getWidth() / 2) - ((this.getWidth() / 2) * this.zoom)) - this.offsetX;
			this.centerY = ((this.getHeight() / 2) - ((this.getHeight() / 2) * this.zoom)) - this.offsetY;

			// Add an extra offset if it exists (for when user is dragging the image)
			if (offsetX !== undefined && offsetY !== undefined) {
				this.centerX -= offsetX;
				this.centerY -= offsetY
			}

			clearCanvas(this.ctx, this.getWidth(), this.getHeight()); // Wipe canvas
			this.ctx.save(); // Save the current translation of the canvas
			this.ctx.translate(this.centerX, this.centerY); // Translate the canvas to match the offset or move to center
			this.ctx.scale(this.zoom, this.zoom);
			this.drawImage();
			this.drawAnnotations();
			this.ctx.restore(); // Restore the original translation
		},

		// Wipe and redraw the image, polygons, and points on the canvas
		drawAnnotations: function () {
			if (this.ctx !== undefined) {

				// Draw the grey highlighted circle on the point the mouse is hovering over (if there is one)
				if (this.hoverPoly !== undefined && this.hoverPt !== undefined) {
					drawPt(this.polygons[this.hoverPoly].points[this.hoverPt], this.ctx, adjustSizeForZoom(8, this.zoom), "gray");
				}

				// Draw the blue highlighted circle on the point the mouse has clicked on (if there is one)
				if ((this.selPoly !== undefined && this.selPt !== undefined) && (this.selPt !== this.hoverPt || this.selPoly !== this.hoverPoly)) {
					drawPt(this.polygons[this.selPoly].points[this.selPt], this.ctx, adjustSizeForZoom(8, this.zoom), "blue");
				}

				// Draw the polygons on the canvas in black, adjusted to the current zoom level, with their respective labels
				for (let i = 0; i < this.polygons.length; ++i) {
					drawPolygon(this.polygons[i].points, this.ctx, "black", this.zoom);
					drawLabel(this.polygons[i], this.ctx, this.zoom, this.getWidth(), this.getHeight());
				}
			}
		},

		// Calculated the amount of zoom for the canvas according to the user's scrollwheel
		zoomCanvas: function (e) {
			e.preventDefault() // Prevent scrolling entire webpage if on Firefox

			// Support different browsers and normalise scrollwheel to single step
			var zoomDelta = (e.deltaY < 0 || e.detail < 0 || e.wheelDelta > 0) ? 1 : -1;
			var newZoom = Math.exp(zoomDelta * 0.05);
			this.zoom = this.zoom * newZoom;

			// Prevent the user from scrolling too far in or out
			if (this.zoom > 32) {
				this.zoom = 32;

				// Prevent changing offset if already at limit
				if (this.zoomLimit) {
					return;
				}
				else this.zoomLimit = true;
			}
			else if (this.zoom < 0.5) {
				this.zoom = 0.5;

				// Prevent changing offset if already at limit
				if (this.zoomLimit) {
					return;
				}
				else this.zoomLimit = true;
			}
			// Not at zoom limit
			else this.zoomLimit = false;

			// Remove the offset to zoom in/out from the center of the image
			this.offsetX *= newZoom;
			this.offsetY *= newZoom;

			//this.drawCanvas();
			this.drawCanvas();
		},

		// Reset the canvas zoom and offset
		resetZoom: function () {
			this.zoom = 1;
			this.offsetX = 0;
			this.offsetY = 0;
			this.drawCanvas();
		},

		drawImage: function () {
			
			// If no image has been added to the canvas yet
			if (img === undefined)
				return;
			
			// If imgRatio hasn't been set, set it to the value required to fit the image in the canvas
			if (this.imgRatio === undefined) {
				var canvasH = this.getHeight();
				var canvasW = this.getWidth();

				if (img.width / img.height > canvasW / canvasH) {
					this.imgRatio = canvasW / img.width;
				}
				else {
					this.imgRatio = canvasH / img.height;
				}

				// Set the canvas size according to the adjusted image size
				this.ctx.canvas.height = img.height * this.imgRatio;
				this.ctx.canvas.width = img.width * this.imgRatio;
			}

			// Draw the image according to the adjusted dimensions for fitting it in the canvas
			this.ctx.drawImage(img, 0, 0, img.width * this.imgRatio, img.height * this.imgRatio);
		},

		// Wipe all polygons from the canvas
		clearPolygons: function () {
			if (this.mode === canvasMode.cDrawPoly) {
				this.finishPoly();
			}
			if (!confirm("This will remove all annotations on your canvas, are you sure you want to continue?")) {
				return;
			}
			
			if (this.hiddenPolygons.length | this.polygons.length)
				this.unsavedChanges = true;

			this.hiddenPolygons = [];
			this.polygons = [];
			this.drawCanvas();
			updateAnnotations(this.polygons, this.hiddenPolygons);
		},

		// Hide a polygon from the canvas
		hidePolygon: function (ID) {
			if (this.mode === canvas.cDrawPoly) {
				this.finishPoly();
			}
			for (let i = 0; i < this.polygons.length; ++i) {
				if (this.polygons[i].ID === ID) {
					this.hiddenPolygons.push(this.polygons[i]);
					this.polygons.splice(i, 1);

					// Sort hidden polygons by ID
					this.hiddenPolygons = bubbleSort(this.hiddenPolygons);
					
					// Clear the current hover/select point in case the polygon is removed
					this.clearHoverSelect();
					
					this.drawCanvas();
					updateAnnotations(this.polygons, this.hiddenPolygons);
					return;
				}
			}

			alert("Visible polygon could not be found.");
		},

		// Show a hidden polygon on the canvas
		showPolygon: function (ID) {
			if (this.mode === canvasMode.cDrawPoly) {
				this.finishPoly();
			}

			for (let i = 0; i < this.hiddenPolygons.length; ++i) {
				if (this.hiddenPolygons[i].ID === ID) {
					this.polygons.push(this.hiddenPolygons[i]);
					this.hiddenPolygons.splice(i, 1);

					// Sort polygons by ID
					this.polygons = bubbleSort(this.polygons);

					// Clear the current hover/select point in case the polygon is removed
					this.clearHoverSelect();

					this.drawCanvas();
					updateAnnotations(this.polygons, this.hiddenPolygons);
					return;
				}
			}

			alert("Hidden polygon could not be found.");
		},
		
		// Delete the most recently drawn point
		deleteLastPt: function () {
			let lastPolygonIdx = this.polygons.length - 1;
			this.polygons[lastPolygonIdx].points.pop();
			if (!this.polygons[lastPolygonIdx].points.length) {
				this.polygons.pop();
				this.mode = canvasMode.cNone;
			}
			this.drawCanvas();
			this.updateAnnotations();
		},
		
		// Delete the most recently drawn polygon
		deleteLastPoly: function () {
			let allPolys = this.getPolygons();
			if (allPolys.length) {
				let ID = allPolys[allPolys.length - 1].ID;
				this.deletePolygon(ID);
			}
		},

		// Delete a polygon
		deletePolygon: function (ID) {
			if (this.mode === canvasMode.cDrawPoly)
				this.finishPoly();
				
			this.unsavedChanges = true;
			

			// Iterate through non-visible polygons (probably the smaller array)
			for (let i = 0; i < this.hiddenPolygons.length; ++i) {
				if (this.hiddenPolygons[i].ID === ID) {
					this.hiddenPolygons.splice(i, 1);

					// Subtract each ID by one to account for missing polygon
					for (let j = i; j < this.hiddenPolygons.length; ++j) {
						this.hiddenPolygons[j].ID -= 1;
					}
					for (let j = 0; j < this.polygons.length; ++j) {
						if (this.polygons[j].ID > ID)
							this.polygons[j].ID -= 1;
					}
					
					// Clear the current hover/select point in case the polygon is removed
					this.clearHoverSelect();
					
					this.drawCanvas();
					updateAnnotations(this.polygons, this.hiddenPolygons);
					return;
				}
			}

			// Iterate through visible polygons
			for (let i = 0; i < this.polygons.length; ++i) {
				if (this.polygons[i].ID === ID) {
					this.polygons.splice(i, 1);

					// Subtract each ID by one to account for missing polygon
					for (let j = i; j < this.polygons.length; ++j) {
						this.polygons[j].ID -= 1;
					}
					for (let j = 0; j < this.hiddenPolygons.length; ++j) {
						if (this.hiddenPolygons[j].ID > ID)
							this.hiddenPolygons[j].ID -= 1;
					}
					
					// Clear the current hover/select point in case the polygon is removed
					this.clearHoverSelect();

					this.drawCanvas();
					updateAnnotations(this.polygons, this.hiddenPolygons);
					return;
				}
			}
		},

		changeClassLabel: function (ID, type, newValue) {
			// Iterate through non-visible polygons (probably the smaller array)
			for (let i = 0; i < this.hiddenPolygons.length; ++i) {
				if (this.hiddenPolygons[i].ID === ID) {
					if (type === "class")
						this.hiddenPolygons[i].class = newValue;
					else
						this.hiddenPolygons[i].label = newValue;
					return;
				}
			}

			// Iterate through visible polygons
			for (let i = 0; i < this.polygons.length; ++i) {
				if (this.polygons[i].ID === ID) {
					if (type === "class")
						this.polygons[i].class = newValue;
					else
						this.polygons[i].label = newValue;
					this.drawCanvas();
					return;
				}
			}

			alert("Polygon could not be found.");
		},

		// Get the height of the canvas in pixels
		getHeight: function () {
			return this.ctx.canvas.height;
		},

		// Get the width of the canvas in pixels
		getWidth: function () {
			return this.ctx.canvas.width;
		},

		// Get the array of polygons
		getPolygons: function () {
			return bubbleSort(this.polygons.concat(this.hiddenPolygons));
		},
		
		// Returns the total number of visible and hidden polygons
		getPolygonsLength: function () {
			return this.polygons.length + this.hiddenPolygons.length;
		},
		
		// Returns a list of the classes in use by annotations
		getClasses: function () {
			let classes = [];
			
			for (let i = 0; i < this.polygons.length; ++i) {
				classes.push(this.polygons[i].class);
			}
			
			for (let i = 0; i < this.hiddenPolygons.length; ++i) {
				classes.push(this.hiddenPolygons[i].class);
			}
			
			// Remove duplicate values
			classes = Array.from(new Set(classes));
			
			return classes;
		},
		
		// Returns a list of the classes in use by annotations
		getLabels: function () {
			let labels = [];
			
			for (let i = 0; i < this.polygons.length; ++i) {
				labels.push(this.polygons[i].label);
			}
			
			for (let i = 0; i < this.hiddenPolygons.length; ++i) {
				labels.push(this.hiddenPolygons[i].label);
			}
			
			// Remove duplicate values
			labels = Array.from(new Set(labels));
			
			return labels;
		},
		
		updateAnnotations: function () {
			updateAnnotations(this.polygons, this.hiddenPolygons);
		},

		// Apply auto-annotations
		autoAnnotate: function (autoAnnotations) {
			let allPolygons = this.getPolygons();
			if (allPolygons.length) {
				for (let i = 0; i < autoAnnotations.length; i++) {
					let newPoly = JSON.stringify(autoAnnotations[i].points);
					for (let j = 0; j < allPolygons.length; ++j) {
						let oldPoly = JSON.stringify(allPolygons[j].points);
						// If auto annotations polygon exists in the current polygons
						if (newPoly === oldPoly) {
							// Remove polygon from auto annotations
							autoAnnotations.splice(i);
							// Fix proceeding ID values
							for (let k = (i > 0) ? i - 1 : 0; k < autoAnnotations.length; k++) {
								autoAnnotations[k].ID -= 1;
							}
							continue;
						}
					}
				}
			}

			if (autoAnnotations.length) {
				this.unsavedChanges = true;

				this.polygons = this.polygons.concat(autoAnnotations);
	
				// Sort the polygons by ID
				this.polygons = bubbleSort(this.polygons);
				
				this.drawCanvas();
				
				checkForNewClassLabels(autoAnnotations);
				this.updateAnnotations();
			}
		},

		addAnnotations: function (annotations) {
			this.polygons = this.polygons.concat(annotations);
			this.drawCanvas();
			this.updateAnnotations();
		},

		// Start dragging an image
		startDragImage: function (x, y) {
			this.mode = canvasMode.cDragImage;
			this.startDragX = x;
			this.startDragY = y;
			this.selPoly = undefined;
			this.selPt = undefined
		},

		// Drag the image with the select tool
		dragImage: function (x, y) {
			var offsetX = (this.startDragX - x);
			var offsetY = (this.startDragY - y);

			this.drawCanvas(offsetX, offsetY);
		},

		// Finish dragging the image
		finishImageDrag: function (x, y) {

			this.offsetX += (this.startDragX - x);
			this.offsetY += (this.startDragY - y)


			this.drawCanvas();

			canvas.mode = canvasMode.cNone;
		},

		// Start drawing a polygon
		startDrawing: function (x, y, labelClass, label, type) {
			this.selPoly = undefined;
			this.selPt = undefined;
			
			// Set the canvas mode
			this.mode = (type === "box") ? canvasMode.cDrawBox : canvasMode.cDrawPoly;

			this.unsavedChanges = true;

			// Adjust mouse coordinates according to canvas page offset and zoom
			x = x / this.zoom + (-this.centerX / this.zoom);
			y = y / this.zoom + (-this.centerY / this.zoom);

			// Keep the point within the image bounds
			x = checkBounds(x, this.getWidth(), 0);
			y = checkBounds(y, this.getHeight(), 0);

			this.polygons.push({ ID: this.polygons.length + this.hiddenPolygons.length + 1, class: labelClass, label: label, points: [[x, y]] });
			this.drawCanvas();
			if (type !== "box")
				updateAnnotations(this.polygons, this.hiddenPolygons);
		},

		// Draw a rectangular bounding box
		drawBox: function (x, y, done) {

			// Adjust mouse coordinates according to canvas page offset and zoom
			x = x / this.zoom + (-this.centerX / this.zoom);
			y = y / this.zoom + (-this.centerY / this.zoom);

			// Get index of current polygon
			let idx = this.polygons.length - 1;
			
			let point1 = this.polygons[idx].points[0];

			// Calculate the x,y for the new point
			var newPoint = { x: 0, y: 0 };
			newPoint.x = checkBounds(x, this.getWidth(), 0); // Keep within image bounds
			newPoint.y = checkBounds(y, this.getHeight(), 0); // Keep within image bounds
			
			// Check if box is 1D and delete if so
			if (done && (newPoint.x === point1[0] && newPoint.y === point1.y)) {
				this.mode = canvasMode.cNone;
				this.polygons.pop();
				this.drawCanvas();
				return;
			}
			
			// Add new points to polygons
			this.polygons[idx].points.push([newPoint.x, point1[1]]);
			this.polygons[idx].points.push([newPoint.x, newPoint.y]);
			this.polygons[idx].points.push([point1[0], newPoint.y]);


			// Draw the bounding box, and remove the three points if the user is still drawing
			this.drawCanvas();

			if (done) {
				// Order points clockwise
				let left = Math.min(point1[0], newPoint.x);
				let right = Math.max(point1[0], newPoint.x);
				let top = Math.min(point1[1], newPoint.y);
				let bottom = Math.max(point1[1], newPoint.y);
				this.polygons[idx].points[0] = [left, top];
				this.polygons[idx].points[1] = [right, top];
				this.polygons[idx].points[2] = [right, bottom];
				this.polygons[idx].points[3] = [left, bottom];
				
				this.mode = canvasMode.cNone;
				updateAnnotations(this.polygons, this.hiddenPolygons);
			}
			else {
				this.polygons[idx].points.pop();
				this.polygons[idx].points.pop();
				this.polygons[idx].points.pop();
			}
		},

		// Draw a polygonal bounding box
		drawPoly: function (x, y) {

			// Adjust mouse coordinates according to canvas page offset and zoom
			var x = x / this.zoom + (-this.centerX / this.zoom);
			var y = y / this.zoom + (-this.centerY / this.zoom);

			// Keep the point within the image bounds
			x = checkBounds(x, this.getWidth(), 0);
			y = checkBounds(y, this.getHeight(), 0);

			// Get index of current polygon
			var idx = this.polygons.length - 1;

			// Avoid creating point if it already exists in the polygon
			for (let i = 0; i < this.polygons[idx].points.length; ++i) {
				if (x === this.polygons[idx].points[i][0] && y === this.polygons[idx].points[i][1])
					return;
			}

			// Push the bounding box to the polygons array and draw it on the canvas
			this.polygons[idx].points.push([x, y]);
			this.drawCanvas();
			updateAnnotations(this.polygons, this.hiddenPolygons);
		},

		// Finish drawing a polygon
		finishPoly: function () {
			if (this.polygons[this.polygons.length - 1].points.length < 3) {
				this.polygons.pop();
				this.drawCanvas();
			}
			this.mode = canvasMode.cNone;
			updateAnnotations(this.polygons, this.hiddenPolygons);
		},

		// Determine which polygon point the mouse is hovering over, if any
		findHoverPt: function (x, y) {

			// Transform mouse coordinates according to canvas zoom and offset
			x = x / this.zoom + (-this.centerX / this.zoom);
			y = y / this.zoom + (-this.centerY / this.zoom);

			var newHoverPt = undefined;
			var newHoverPoly = undefined;
			var pointFound = false;

			// Adjust selection area according to zoom level
			var selectionRadius = adjustSizeForZoom(11, this.zoom);

			// While a point has not been found, continue searching through the polygons and points
			for (let i = 0; i < this.polygons.length && !pointFound; ++i) {
				for (let j = 0; j < this.polygons[i].points.length; ++j) {
					if (dist(this.polygons[i].points[j], [x, y]) <= selectionRadius) {
						newHoverPoly = i;
						newHoverPt = j;
						pointFound = true;
						break;
					}
				}
			}

			// If the found point is not already hovered over, highlight it on the canvas
			if (newHoverPoly !== this.hoverPoly || newHoverPt !== this.hoverPt) {
				this.hoverPt = newHoverPt;
				this.hoverPoly = newHoverPoly;
				this.drawCanvas();
			}
		},

		// Check if mouse is hovering over a point
		getHoverStatus: function () {
			if (this.hoverPt !== undefined) {
				return true;
			}
			else {
				return false;
			}
		},

		// Start dragging a polygon point
		beginDragPt: function () {
			this.mode = canvasMode.cDragPt;
			this.selPoly = this.hoverPoly;
			this.selPt = this.hoverPt;
		},

		// Drag a polygon point
		moveSelPt: function (x, y) {

			// Transform mouse coordinates according to canvas zoom and offset
			x = x / this.zoom + (-this.centerX / this.zoom);
			y = y / this.zoom + (-this.centerY / this.zoom);

			// Keep the point within the image bounds
			x = checkBounds(x, this.getWidth(), 0);
			y = checkBounds(y, this.getHeight(), 0);

			// If a point is selected, move it to the new location
			if (this.selPt !== undefined) {
				this.polygons[this.selPoly].points[this.selPt][0] = x;
				this.polygons[this.selPoly].points[this.selPt][1] = y;
				this.drawCanvas();
			}
		},

		// Finish dragging a polygon point
		endDragPt: function () {
			if (this.mode === canvasMode.cDragPt) {
				this.mode = canvasMode.cNone;
				this.selPoly = undefined;
				this.selPt = undefined;
			}
		},

		// Select a polygon point and highlight it on the canvas
		selectPt: function () {
			this.mode = (this.hoverPoly !== undefined && this.hoverPt !== undefined) ? canvasMode.cSelPt : canvasMode.cNone;
			this.selPoly = this.hoverPoly;
			this.selPt = this.hoverPt;
			this.drawCanvas();
		},

		// Delete a polygon point
		deletePt: function () {
			if (this.mode === canvasMode.cSelPt && this.selPoly !== undefined && this.selPt !== undefined) {

				// If there are less than 3 points remaining, remove polygon and update IDs
				if (this.polygons[this.selPoly].points.length < 3) {
					let deletedID = this.polygons[this.selPoly].ID;
					this.polygons.splice(this.selPoly);
					for (let i = 0; i < this.polygons.length; ++i) {
						if (this.polygons[i].ID > deletedID)
							this.polygons[i].ID -= 1;
					}
					for (let i = 0; i < this.hiddenPolygons.length; ++i) {
						if (this.hiddenPolygons[i].ID > deletedID)
							this.hiddenPolygons[i].ID -= 1;
					}
					updateAnnotations();
				}
				else {
					this.polygons[this.selPoly].points.splice(this.selPt, 1);
				}

				if (this.hoverPt === this.selPt) {
					this.hoverPoly = undefined;
					this.hoverPt = undefined;
				} else if (this.hoverPt > this.selPt) {
					--this.hoverPt;
				}
				this.selPoly = undefined;
				this.selPt = undefined;
				this.drawCanvas();
			}
		},
		
		clearHoverSelect: function () {
			this.selPoly = undefined;
			this.selPt = undefined;
			this.hoverPoly = undefined;
			this.selPoly = undefined;
		}
	};
}

// Adjust the coordinates returned from the ML model
function transformAnnotations(response) {
	var autoAnnotations = [];
	var existingLength = canvas.getPolygonsLength();
	canvasWidth = canvas.getWidth();
	canvasHeight = canvas.getHeight();
	for (let i = 0; i < response.image.annotations.length; ++i) {
		var label = response.image.annotations[i].label;
		var confidence = response.image.annotations[i].confidence;
		autoAnnotations.push({ ID: existingLength + 1 + i, class: "undefined", label: label, confidence: confidence, points: [] });
		var x = response.image.annotations[i].x;
		var y = response.image.annotations[i].y;
		var w = response.image.annotations[i].w;
		var h = response.image.annotations[i].h;
		var points = [
			{ x: checkBounds(x - (w / 2), 1, 0), y: checkBounds(y - (h / 2), 1, 0) },
			{ x: checkBounds(x + (w / 2), 1, 0), y: checkBounds(y - (h / 2), 1, 0) },
			{ x: checkBounds(x + (w / 2), 1, 0), y: checkBounds(y + (h / 2), 1, 0) },
			{ x: checkBounds(x - (w / 2), 1, 0), y: checkBounds(y + (h / 2), 1, 0) }];
		for (let j = 0; j < points.length; ++j) {
			points[j].x *= canvasWidth;
			points[j].y *= canvasHeight;
			autoAnnotations[i].points.push([points[j].x, points[j].y]);
		}
	}
	canvas.autoAnnotate(autoAnnotations);
}

// Bubble sort algorithm to sort polygons into ascending order by ID
function bubbleSort(polygons) {
	for (let i = 0; i < polygons.length; ++i) {
		for (let j = 0; j < polygons.length - i - 1; ++j) {
			if (polygons[j + 1].ID < polygons[j].ID) {
				// Swap polygons
				[polygons[j + 1], polygons[j]] = [polygons[j], polygons[j + 1]];
			}
		}
	}
	return polygons;
}

// Normalise y coordinate (0-1)
function normaliseHeight(y) {
	return (y / canvas.getHeight());
}

// Normalise x coordinate (0-1)
function normaliseWidth(x) {
	return (x / canvas.getWidth());
}

// Make sure a value is within the given bounds
function checkBounds(val, max, min) {
	if (val > max)
		return max;

	else if (val < min)
		return min;

	else
		return val;
}

// Return the offset required to keep a value within given bounds
function checkOffset(val, max, min) {
	if (val > max)
		return -(val - max);

	else if (val < min)
		return -(val - min);

	else
		return 0;
}

// Remove polygons from the canvas
function clearPolygons() {
	canvas.clearPolygons();
}

function resetZoom() {
	canvas.resetZoom();
}

// Draw a point on a canvas
function drawPt(pt, ctx, radius, color) {
	// Enclosed within try statement in case given polygons are not up to date
	try {
		ctx.beginPath();
		ctx.arc(pt[0], pt[1], radius, 0, Math.PI * 4);
		ctx.fillStyle = color;
		ctx.fill();
	}
	catch {
		return;
	}
	
}


// Draw polygons on a canvas
function drawPolygon(pts, ctx, color, zoom) {

	// Draw the points in the polygons, adjusted to fit the canvas zoom
	for (let i = 0; i < pts.length; ++i) {
		drawPt(pts[i], ctx, adjustSizeForZoom(4, zoom), color);
	}

	// Draw the paths between the polygon points
	ctx.beginPath();
	ctx.moveTo(pts[0][0], pts[0][1]);

	for (let i = 1; i < pts.length; ++i) {
		ctx.lineTo(pts[i][0], pts[i][1]);
	}

	ctx.lineTo(pts[0][0], pts[0][1]);
	ctx.strokeStyle = color;
	ctx.lineWidth = adjustSizeForZoom(2, zoom);
	ctx.fillStyle = "rgba(255, 255, 255, 0.1)";
	ctx.fill();
	ctx.stroke();
}

// Draw a label on the highest, left-most point on a polygon
function drawLabel(polygon, ctx, zoom, boundX, boundY) {

	// Check if confidence exists
	var confidence = (polygon.confidence) ? polygon.confidence.toFixed(2).toString() + " | " : "";
	var text = polygon.ID.toString() + " | " + confidence + polygon.label;

	// Adjust font size and padding for zoom
	var fontSize = adjustSizeForZoom(14, zoom);
	var paddingSize = adjustSizeForZoom(3, zoom);

	var xOffset = 0;
	var yOffset = 0;

	var point = [boundX, boundY];

	for (let i = 0; i < polygon.points.length; ++i) {
		// If polygon point is higher, or equal in height but further left
		if (polygon.points[i][1] < point[1] || (polygon.points[i][1] === point[1] && polygon.points[i][0] < point[0])) {
			point = Array.from(polygon.points[i]); // Create clone rather than reference to original array
		}
	}

	point[1] -= adjustSizeForZoom(10, zoom); // Move text slightly above point
	ctx.font = fontSize.toString() + "px Arial";

	// Adjust rectangle to start at descender height of Arial font
	var rectCoords = { x1: point[0] - paddingSize, y1: point[1] + (0.22424 * fontSize) + paddingSize, x2: ctx.measureText(text).width + paddingSize * 2, y2: -fontSize - paddingSize * 2 };

	// Keep label within image bounds
	xOffset = checkOffset(rectCoords.x1, boundX, 0);
	if (xOffset === 0)
		xOffset = checkOffset(rectCoords.x1 + rectCoords.x2, boundX, 0);
	yOffset = checkOffset(rectCoords.y1 + rectCoords.y2, boundY, 0);

	if (yOffset === 0)
		yOffset = checkOffset(rectCoords.y1, boundY, 0);
	if (xOffset != 0 || yOffset != 0) {
		point[0] += xOffset;
		point[1] += yOffset;
		rectCoords = { x1: point[0] - paddingSize, y1: point[1] + (0.22424 * fontSize) + paddingSize, x2: ctx.measureText(text).width + paddingSize * 2, y2: -fontSize - paddingSize * 2 };
	}

	// Draw rectangle
	ctx.fillStyle = "black";
	ctx.fillRect(rectCoords.x1, rectCoords.y1, rectCoords.x2, rectCoords.y2);

	// Draw text
	ctx.fillStyle = "white";
	ctx.fillText(text, point[0], point[1]);
}

// Clear a canvas of image and annotations
function clearCanvas(ctx, width, height) {
	ctx.clearRect(0, 0, width, height);
}

// Shift a polygon point's coordinates
function shiftPt(pt, shift) {
	return [pt[0] + shift[0], pt[1] + shift[1]];
}

// Adjust the size of a value according to the canvas zoom level
function adjustSizeForZoom(num, zoom) {
	return num * (1 / zoom);
}

// Return the square of a value
function sq(x) {
	return x * x;
}

// Get the distance between two coordinates
function dist(pt1, pt2) {
	if (pt2 === undefined) {
		pt2 = [0, 0];
	}

	var pt = shiftPt(pt1, [-pt2[0], -pt2[1]]);

	return sq(pt[0]) + sq(pt[1]);
}

// React to the user moving the mouse
function mouseMoveHandler(e) {

	// Drag a polygon point
	if (canvas.mode === canvasMode.cDragPt) {
		canvas.moveSelPt(e.offsetX, e.offsetY);
	}

	// Draw a rectangular bounding box
	else if (canvas.mode === canvasMode.cDrawBox) {
		canvas.drawBox(e.offsetX, e.offsetY, false);
	}

	else if (canvas.mode === canvasMode.cDragImage) {
		canvas.dragImage(e.x, e.y);
	}

	// Check if the user is hovering over a polygon point
	else {
		if (currentFilename)
			canvas.findHoverPt(e.offsetX, e.offsetY);
	}
}

// React to the user clicking the mouse down
function mouseDownHandler(e) {
	var toolSelected = getSelectedTool();
	var labelSelected = getSelectedLabel();
	var classSelected = getSelectedClass();

	// Start dragging a polygon point
	if (toolSelected === "select") {
		if(canvas.mode === canvasMode.cDrawPoly) {
			canvas.finishPoly();
		}
		if (canvas.getHoverStatus()) {
			canvas.beginDragPt();
		}
		else {
			if (currentFilename)
				canvas.startDragImage(e.x, e.y);
			else
				alert("Select or upload an image first!");
		}
	}

	// Start drawing a rectangular bounding box
	else if (toolSelected === "box" && labelSelected !== null) {
		if(canvas.mode === canvasMode.cDrawPoly) {
			canvas.finishPoly();
		}
		if (currentFilename)
			canvas.startDrawing(e.offsetX, e.offsetY, classSelected, labelSelected, "box");
		else
			alert("Select or upload an image first!");
	}
}

// React to the user finishing a mouse click
function mouseUpHandler(e) {

	// Finish dragging a polygon point
	if (canvas.mode === canvasMode.cDragPt) {
		canvas.endDragPt();
	}

	// Finish drawing a rectagular bounding box
	else if (canvas.mode === canvasMode.cDrawBox) {
		canvas.drawBox(e.offsetX, e.offsetY, true);
	}

	else if (canvas.mode === canvasMode.cDragImage) {
		canvas.finishImageDrag(e.x, e.y);
	}
}

// React to the user clicking the mouse
function mouseClickHandler(e) {
	var toolSelected = getSelectedTool();
	var labelSelected = getSelectedLabel();
	var classSelected = getSelectedClass();

	// Start or continue drawing a polygon
	if (toolSelected === "polygon" && labelSelected !== null) {
		if (canvas.mode !== canvasMode.cDrawPoly) {
			if (currentFilename)
				canvas.startDrawing(e.offsetX, e.offsetY, classSelected, labelSelected, "polygon");
			else
				alert("Select or upload an image first!");
		}
		else {
			canvas.drawPoly(e.offsetX, e.offsetY);
		}
	}

	// Select a polygon point
	else if (toolSelected === "select" && canvas.getHoverStatus()) {
		if(canvas.mode === canvasMode.cDrawPoly) {
			canvas.finishPoly();
		}
		canvas.selectPt();
	}
}

// React to the mouse moving outside the canvas
function mouseOutHandler(e) {
	if (canvas.mode === canvasMode.cDragImage) {
		canvas.finishImageDrag(e.clientX, e.clientY);
	}
}

// Zoom the canvas when the user uses the scrollwheel
function mouseWheelHandler(e) {
	if (currentFilename)
		canvas.zoomCanvas(e);
	else
		alert("Select or upload an image first!");
}

// React to the user pressing a keyboard key
function keyUpHandler(e) {
	if (e.key === "Delete" || (e.key === "Backspace" && !isTyping)) {
		if (canvas.selPt !== undefined && canvas.selPoly !== undefined)
			canvas.deletePt();
		else if (canvas.mode === canvasMode.cDrawPoly)
			canvas.deleteLastPt();
		else
			canvas.deleteLastPoly();
	}

	else if (e.key === "Enter") {
		if(canvas.mode === canvasMode.cDrawPoly) {
			canvas.finishPoly();
		}
	}

	else if (e.key === "ArrowLeft") {
		if (canvas.unsavedChanges) {
			if (!confirm("The current image has unsaved changes, are you sure you want to continue?"))
				return;
		}
		changeImage(-1);
	}

	else if (e.key === "ArrowRight") {
		if (canvas.unsavedChanges) {
			if (!confirm("The current image has unsaved changes, are you sure you want to continue?"))
				return;
		}
		changeImage(1);
	}
}

// Check which tool the user has selected
function getSelectedTool() {
	if (document.getElementById("button-box").checked) {
		return "box";
	}
	else if (document.getElementById("button-polygon").checked) {
		return "polygon";
	}
	else if (document.getElementById("button-select").checked) {
		return "select";
	}
	return null;
}

// Returns the class the user has selected
function getSelectedClass() {
	let select = document.getElementById('selectable-classes');
	var selectedIndex = select.selectedIndex;
	var selection = "undefined";

	if (selectedIndex !== -1)
		selection = select.options[select.selectedIndex].value;
	else
		addNewClass("undefined", false);

	return selection;
}

// Returns all the classes available to select
function getAllClasses() {
	let classDropdown = document.getElementById('selectable-classes');
	return classDropdown.options;
}

// Check which label the user has selected
function getSelectedLabel() {
	var select = document.getElementById('selectable-labels')
	var selectedIndex = select.selectedIndex;
	var selection = "undefined";

	if (selectedIndex !== -1)
		selection = select.options[select.selectedIndex].value;
	else
		addNewLabel("undefined", false);

	return selection;
}

// Returns all the labels available to select
function getAllLabels() {
	let labelDropdown = document.getElementById('selectable-labels');
	return labelDropdown.options;
}

// Add a new class to the available classes
function addNewClass(className, autoAnnotate) {
	// Make sure class is under 31 characters
	if (className.length > 30) {
		alert ("Class name must be 30 characters or less.");
		return;
	}

	// Prevent adding an existing class and prepare list of total classes
	let allClasses = getAllClasses();
	let newClasses = [];
	for (let i = 0; i < allClasses.length; ++i) {
		if (className === allClasses[i].value) {
			if (!autoAnnotate)
				alert("Class name already exists.");
			return;
		}
		newClasses.push(allClasses[i].value);
	}

	newClasses.push(className);

	refreshClassLabelDropdown(newClasses, "classes");

	if (!autoAnnotate)
		uploadClassLabels();
}

// Add a new label to the available labels
function addNewLabel(labelName, autoAnnotate) {
	// Make sure label is under 31 characters
	if (labelName.length > 30) {
		alert ("Label name must be 30 characters or less.");
		return;
	}

	// Prevent adding an existing label and prepare list of total labels
	let allLabels = getAllLabels();
	let newLabels = [];
	for (let i = 0; i < allLabels.length; ++i) {
		if (labelName === allLabels[i].value) {
			if (!autoAnnotate)
				alert("Label name already exists.");
			return;
		}
		newLabels.push(allLabels[i].value);
	}

	newLabels.push(labelName);

	refreshClassLabelDropdown(newLabels, "labels");

	if (!autoAnnotate)
		uploadClassLabels();
}

// Update the class or label selection dropdown
function refreshClassLabelDropdown(values, type) {

	// Sort classes or labels alphabetically
	values.sort();

	let dropdown = document.getElementById('selectable-' + type);
	dropdown.innerHTML = "";

	for (let i = 0; i < values.length; ++i) {
		let newValue = document.createElement('option');
		newValue.text = values[i];
		newValue.value = values[i];
		dropdown.append(newValue);
	}

	canvas.updateAnnotations();
	updateClassLabels();
}

// Adds any new classes or labels returned from the ML model
function checkForNewClassLabels(annotations) {
	for (let i = 0; i < annotations.length; ++i) {
		let className = annotations[i].class;
		let labelName = annotations[i].label;
		addNewClass(className, true);
		addNewLabel(labelName, true);
	}
	updateClassLabels();
}

window.onload = async function () {
	await setupPage()
};
