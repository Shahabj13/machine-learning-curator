sudo apt update -y

##### package dependencies #####

#cd UI_Gateway_API
sudo apt-get install -y postgresql postgresql-contrib
sudo apt-get install -y npm


##### npm dependencies #####

#sudo npm install knex
sudo npm install -g pg
sudo npm install -g browserify


##### database commands #####

# knex login user table creation
sudo -u postgres psql -c "create user shieldtec_user with encrypted password 'DBG2iasU8RMR'"
sudo -u postgres psql -c "create database shieldtec"
sudo -u postgres PGPASSWORD='DBG2iasU8RMR' psql shieldtec_user -h 127.0.0.1 -d shieldtec -f create_tables.sql


##### sudo rm node_modules/connection

# prevent an error from being thrown on some installations
sudo rm node_modules/connection
sudo mkdir node_modules/connection

sudo npm install
sudo npm run dev
