const Pool = require('pg').Pool;

const connectionString = process.env.CONNECTION_STRING;

const pool = new Pool({
  connectionString: connectionString,
  ssl: {
    rejectUnauthorized: false,
  },
});

// return all annotations for a given image in the database
const getAnnotations = async (req, res) => {
  const image_filename = req.params.image_filename;
  try {
    const client = await pool.connect();
    await client.query('SELECT * FROM annotation WHERE image_filename = $1', [image_filename]).then(result => {
      const results = { "response": "success", 'results': (result) ? result.rows : null };
      res.status(200).send(results);
    });
    client.release();
  }
  catch (error) {
    console.error(error);
    res.status(201).json({ "response": "error", "message": "could not retrieve annotations from database" });
  }
}

// return all images in the database
const getImages = async (req, res) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM image').then(result => {
      const results = { 'response': 'success', 'filenames': (result) ? result.rows : null };
      res.status(200).json(results);
    });
    client.release();
  }
  catch (error) {
    console.error(error);
    res.json({ 'response': 'error', 'message': "could not retrieve filenames from database" });
  }
}

// delete image
const deleteImageByName = async (req, res) => {
  try {
    const image_filename = req.params.image_filename;
    const client = await pool.connect();
    await client.query('DELETE FROM image WHERE image_filename = $1', [image_filename]).then(() => {
      res.status(200).json({ "response": "success" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    res.status(201).json({ "response": "error", "message": "could not delete image from database" });
  }
}

// delete label
const deleteLabelByName = async (req, res) => {
  try {
    const label_name = req.params.label_name;
    const client = await pool.connect();
    await client.query('DELETE FROM label WHERE label_name = $1', [label_name]).then(() => {
      res.status(200).json({ "response": "success" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    if (error.code === "23503") // Violation of foreign key constraint
      error = "label is in use by another image's annotations";
    else
      error = "could not delete label from database";
    res.status(201).json({ "response": "error", "message": error });
  }
}

// delete class
const deleteClassByName = async (req, res) => {
  try {
    const class_name = req.params.class_name;
    const client = await pool.connect();
    await client.query("DELETE FROM class WHERE class_name = $1", [class_name]).then(() => {
      res.status(200).json({ "response": "success" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    if (error.code === "23503") // Violation of foreign key constraint
      error = "Class is in use by another image's annotations";
    else
      error = "could not delete class from database";
    res.status(201).json({ "response": "error", "message": error });
  }
}

// update a user by a given user ID
const updateUserByID = async (email, time, id) => {
  try {
    const client = await pool.connect();
    await client.query('UPDATE login_user SET email = $1, login_time=$2 WHERE id = $3', [email, time, id]).then(() => {
      return true;
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    return false;
  }
}

// delete a user by a given user ID
const deleteUser = async (request, response) => {
  try {
    const id = parseInt(request.params.id)

    const client = await pool.connect();
    await client.query('DELETE FROM users WHERE id = $1', [id]).then(() => {
          response.status(200).json({ "response": "success", "message": "User deleted with ID: ${id}" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    res.json({ "response": "error", "message": "could not delete user from database" });
  }
}

// create a new image entry in the DB
const createImage = async (req, res) => {
  try {
    const new_image_filename = req.body.image_filename;
    const new_original_filename = req.body.original_filename;
  
    const client = await pool.connect();
    await client.query('INSERT INTO image (image_filename, original_filename) VALUES ($1, $2)', [new_image_filename, new_original_filename]).then(() => {
      res.status(201).json({ "response": "success" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    if (error.code === "23505")
      error = "image already exists in database";
    else
      error = "could not add image information to database";
    res.json({ "response": "error", "message": error });
  }
}

// create a new class entry in the DB
const createClass = async (req, res) => {
  try {
    const new_class = req.body.class_name;

    const client = await pool.connect();
    await client.query('INSERT INTO class (class_name) VALUES ($1)', [new_class]).then(() => {
      res.status(201).json({ "response": "success" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    res.json({ "response": "error", "message": "class could not be added to database" });
  }
}

// create a new label entry in the DB
const createLabel = async (req, res) => {
  try {
    const new_label = req.body.label_name;

    const client = await pool.connect();
    await client.query('INSERT INTO label (label_name) VALUES ($1)', [new_label]).then(() => {
      res.status(201).json({ "response": "success" });
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    res.json({ "response": "error", "message": "label could not be added to database" });
  }
}

// get all labels in label table
const getAllLabels = async (req, res) => {
  try {
    const client = await pool.connect();
    await client.query('SELECT label_name FROM label').then(result => {
      const results = { "response": "success", 'results': (result) ? result.rows : null };
      res.status(200).json(results);
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    res.json({ "response": "error", "message": "could not retrieve labels from database" });
  }
}

// return all classes in the class table
const getAllClasses = async (req, res) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT class_name FROM class').then(result => {
      const results = { "response": "success", 'results': (result) ? result.rows : null };
      res.status(200).json(results);
    });
    client.release();
  }
  catch (error) {
    console.log(error);
    res.json({ "response": "error", "message": "could not retrieve classes from database" });
  }
}

// search DB
const searchDatabase = async (req, res) => {
  try {
    const search_term = req.params.search_term;
    const client = await pool.connect();

    // get all image_filenames that have original_filenames that contain search term
    const imageResults = await client.query("SELECT image_filename FROM image WHERE original_filename LIKE '%' || $1 || '%';", [search_term]);
    // over time, image_filename results get pushed to this array
    let output = imageResults.rows;

    // get all image_filenames that are associated with users that contain search term
    const emailResults = await client.query("SELECT image_filename FROM image WHERE image_filename IN (SELECT i.image_filename FROM image i, login_user lu, annotation a WHERE i.image_filename = a.image_filename AND a.curator_id = lu.id AND lu.email LIKE '%' || $1 || '%');", [search_term]);
    
    output = output.concat(emailResults.rows);
    
    // get all image_filenames with labels that match search term
    const labelResults = await client.query("SELECT image_filename FROM image WHERE image_filename IN (SELECT i.image_filename FROM image i, label l, annotation a WHERE i.image_filename = a.image_filename AND a.label_name = l.label_name AND l.label_name LIKE '%' || $1 || '%');", [search_term]);
    output = output.concat(labelResults.rows);
    
    // get all image_filenames with classes that match search term
    const classResults = await client.query("SELECT image_filename FROM image WHERE image_filename IN (SELECT i.image_filename FROM image i, class c, annotation a WHERE i.image_filename = a.image_filename AND a.class_name = c.class_name AND c.class_name LIKE '%' || $1 || '%');", [search_term]);
    output = output.concat(classResults.rows);
    
    // get unique list of image_filenames
    let result = [];
    output.forEach((img_filename) => {
      if (!result.includes(img_filename.image_filename)) {
        result.push(img_filename.image_filename);
      }
    });

    // can then iterate througn this result array via result[i]

    res.status(201).json({ "response": "success", "results": result}); //return list of image_filenames
    client.release();
  }
  catch (error) {
    res.status(201).json({ "response": "error", "message": "could not retrieve searched filenames from database" });
  }
}

module.exports = {
  getImages,
  deleteUser,
  updateUserByID,
  getAllClasses,
  getAllLabels,
  createImage,
  createClass,
  createLabel,
  getAnnotations,
  deleteImageByName,
  deleteLabelByName,
  deleteClassByName,
  searchDatabase
}
