BEGIN;
-- create image table
CREATE TABLE IF NOT EXISTS image (
    image_filename VARCHAR(70) PRIMARY KEY NOT NULL,
    original_filename VARCHAR(70) NOT NULL
);

-- create label table
CREATE TABLE IF NOT EXISTS label (
    label_name VARCHAR(30) PRIMARY KEY
);

-- create class table
CREATE TABLE IF NOT EXISTS class (
    class_name VARCHAR(30) PRIMARY KEY
);

-- create login_user table
CREATE TABLE IF NOT EXISTS login_user (
    id serial PRIMARY KEY,
    email VARCHAR(255) NOT NULL UNIQUE,
    password_digest VARCHAR(255) NOT NULL,
    login_time TIMESTAMP DEFAULT NOW()
);

-- create annotation table
CREATE TABLE IF NOT EXISTS annotation (
    annotation_id serial PRIMARY KEY,
    points json NOT NULL,
    mod_time TIMESTAMP NOT NULL,
    curator_id INT NOT NULL,
    image_filename VARCHAR(70) NOT NULL,
    label_name VARCHAR(30) NOT NULL,
    class_name VARCHAR(30) NOT NULL,
    FOREIGN KEY (curator_id)
        REFERENCES login_user (id),
    FOREIGN KEY (image_filename)
        REFERENCES image (image_filename) ON DELETE CASCADE,
    FOREIGN KEY (label_name)
        REFERENCES label (label_name),
    FOREIGN KEY (class_name)
        REFERENCES class (class_name)
);

COMMIT;
