var express = require('express');
var app = express();

const dotenv = require('dotenv');
dotenv.config();

const port = process.env.APP_PORT;

app.use(express.static('public'));

// Import routes
app.use(require('./routers'));

var server = app.listen(port, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log("Annotating app listening at http://%s:%s", host, port)

});