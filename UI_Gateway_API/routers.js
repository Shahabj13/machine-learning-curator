const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
var router = express.Router();
const Pool = require('pg').Pool;
const connectionString = process.env.CONNECTION_STRING;
const pool = new Pool({
    connectionString: connectionString,
    ssl: {
        rejectUnauthorized: false,
    },
});
const passportJWT = require('passport-jwt');
const JwtStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const knex = require('knex');
const jwt = require('jsonwebtoken');
var env = process.env.NODE_ENV || 'development';
const configure = require('./config.js')[env];
const knexDB = knex({
    client: configure.client,
    connection: {
        database: configure.database,
        user: configure.user,
        host: configure.host,
        password: configure.password,
        port: configure.port,
        ssl: {
            rejectUnauthorized: false
        }
    }
});
const db = require('./queries');
const { expressCspHeader, INLINE, NONE, SELF, UNSAFE_INLINE, ALLOW_SCRIPTS, UNSAFE_URL, DATA, BLOB } = require('express-csp-header');
const cors = require('cors');
const path = require('path');
const alert = require('alert');
const bookshelf = require('bookshelf');
const securePassword = require('bookshelf-secure-password');
const bs = bookshelf(knexDB);
const imageServer = require('./connect_To_Image_server');
const mlServer = require('./connect_To_ML');
const multer = require('multer')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './upload')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})
var upload = multer({ storage: storage });

var currentUser = undefined;

bs.plugin(securePassword);

const User = bs.Model.extend({
    tableName: 'login_user',
    hasSecurePassword: true,
});

const { response } = require('express');

router.use(expressCspHeader({
    directives: {
        'default-src': [SELF],
        'script-src': [SELF, INLINE, UNSAFE_INLINE, ALLOW_SCRIPTS],
        'style-src': [SELF],
        'img-src': [SELF, DATA, BLOB],
        'worker-src': [NONE],
        'block-all-mixed-content': true,
        'base-uri': [SELF],
    }
}));

router.use(cors());

router.use(express.json({ limit: '50mb' }));
router.use(express.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }));

// let express to access to static folders to be served in the static files like index.html
router.use(express.static('static'));
//router.use(express.static('view')); // Commented out to prevent accessing static html directly
router.use(express.static(__dirname + '/public'));
router.use('/images', express.static(__dirname + '/public/images')); // redirect to images folder
router.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); // redirect CSS bootstrap
router.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js')); // redirect js bootstrap
router.use('/dist', express.static(__dirname + '/node_modules/jquery/dist/')); // redirect CSS jquery
router.use('/core', express.static(__dirname + '/node_modules/@uppy/core'));
router.use('/dashboard', express.static(__dirname + '/node_modules/@uppy/dashboard'));
router.use('/uppy', express.static(__dirname + '/node_modules/uppy'));
router.use(express.static('bundle'));
router.use(express.static('dist'));
router.use('/Annotate_lib', express.static(__dirname + '/Annotate_lib'));
router.use('/uploads', express.static('upload'));



/*** JWT Authentication Configuration ***/

const ops = {
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET_OR_KEY
}

const strategy = new JwtStrategy(ops, (payload, next) => {
    const user = User.forge({ id: payload.id }).fetch().then(res => {
        next(null, res);
    });
});

function checkToken(reqCookie) {
    try {
        const token = reqCookie.match('(^|;)\\s*' + 'Authorization' + '\\s*=\\s*([^;]+)').pop();
        jwt.verify(token, process.env.SECRET_OR_KEY, (err) => {
            if (err) {
                throw "Invalid token";
            }
            return true;
        });
    }
    catch {
        return false;
    }
}

const authenticateJWT = (req, res, next) => {
    const auth = checkToken(req.headers.cookie);
    if (auth === false)
        res.sendStatus(401);
    else
        next();
};



/*** Website Routes ***/

// Login page
router.get('/', (req, res) => {
    if (checkToken(req.headers.cookie) === false)
        res.sendFile(path.join(__dirname + '/view/index.html'));
    else
        res.redirect("/annotate");    
});

router.get('/accept-disclaimer', (req, res) => {
    res.cookie("AcceptedDisclaimer", "True", {expires: new Date(Date.now() + 8760 * 60 * 60 * 1000)});
    res.redirect("/");
});

// Signup route
router.post('/signup', (req, res) => {
    if (!req.body.email || !req.body.password) {
        return res.status(401).redirect('/signup');
    }
    const curator = new User({
        email: req.body.email,
        password: req.body.password,
        login_time: new Date().toISOString()
    });
    curator.save().then(() => {
        res.redirect('/');
    }).catch(err => {
        res.redirect('/?e=1');
    });
});

// Login route
router.post('/login', (req, res, next) => {
    if (!req.body.email || !req.body.password) {
        return res.status(401).send('password failed');
    }

    User.forge({ email: req.body.email }).fetch().then(result => {
        if (!result) {
            res.redirect('/?e=2');
        }
        result.authenticate(req.body.password).then(user => {
            const payload = { id: user.id };
            const token = jwt.sign(payload, process.env.SECRET_OR_KEY);
            const login_time = new Date().toISOString();
            // Set auth timeout to 30 days if checkbox is checked, else 12 hours
            const authTimeout = (req.body.persistcheck) ? 
                new Date(Date.now() + 720 * 60 * 60 * 1000) : 
                new Date(Date.now() + 12 * 60 * 60 * 1000);
            currentUser = user.id;
            db.updateUserByID(req.body.email, login_time, user.id);
            res.append('setTimeout', 1200);
            res.cookie("Authorization", token.toString(), {expires: authTimeout});

            // Show settings page if 'show settings after login' is checked, otherwise go to annotate page
            if (req.headers.cookie.indexOf('HideLoginSettings=True') === -1)
                res.redirect("/settings");
            else
                res.redirect("/annotate");

        }).catch(err => {
            res.redirect('/?e=3');
        });
    });
});

// Annotation page
router.get('/annotate', authenticateJWT, (req, res) => {
    res.sendFile(path.join(__dirname + '/view/annotate.html'));
});

// Settings page
router.get('/settings', authenticateJWT, (req, res) => {
    res.sendFile(path.join(__dirname + '/view/setting.html'));
});

// Logout route
router.get('/logout', (req, res) => {
    res.clearCookie("Authorization");
    res.redirect('/');
});

// Set config route
router.post('/set-config', authenticateJWT, (req, res) => {
    // Update configuration object
    configure.image_server_ip = req.body.image_ip.split(":")[0];
    configure.image_server_port = req.body.image_ip.split(":")[1]
    configure.ml_server_ip = req.body.ml_ip.split(":")[0];
    configure.ml_server_port = req.body.ml_ip.split(":")[1]

    // Continue to show settings page after login if persist checkbox is checked
    if (req.body.showSettingsCheck && req.headers.cookie.indexOf('HideLoginSettings=True') !== -1)
        res.clearCookie("HideLoginSettings");
    // Prevent showing settings after login if persist checkbox is unchecked
    else if (!req.body.showSettingsCheck && req.headers.cookie.indexOf('HideLoginSettings=True') === -1)
        res.cookie("HideLoginSettings", "True", {expires: new Date(Date.now() + 8760 * 60 * 60 * 1000)});

    res.redirect('/annotate');
});



/*** Database Routes ***/

// search DB for a particular term (searches original image name, curator email, labels, and classes)
router.get('/api/search/:search_term', authenticateJWT, db.searchDatabase);

// get images
router.get('/api/images', authenticateJWT, db.getImages);

// insert new filename and original_filename
router.post('/api/images', authenticateJWT, db.createImage);

// delete an image and all associated annotations
router.delete('/api/images/:image_filename', authenticateJWT, db.deleteImageByName);

// users
router.delete('/users/:id', authenticateJWT, db.deleteUser);

// get a list of all classes or labels
router.get('/api/classes', authenticateJWT, db.getAllClasses);
router.get('/api/labels', authenticateJWT, db.getAllLabels);

// create a class or label
router.post('/api/classes', authenticateJWT, db.createClass);
router.post('/api/labels', authenticateJWT, db.createLabel);

// delete a class/label/image
router.delete('/api/classes/:class_name', authenticateJWT, db.deleteClassByName);
router.delete('/api/labels/:label_name', authenticateJWT, db.deleteLabelByName);

// annotations
const storePoints = async (req, res) => {
    try {
        const annotations = req.body.annotations;
        const curator_id = currentUser;
        const image_filename = req.body.image_filename;
        const num_annotations = annotations.length;

        if(!curator_id) {
            throw "Curator ID could not be found. Please log out and log back in.";
        }

        const client = await pool.connect();
        await client.query("BEGIN;");

        // delete all existing annotations and start transaction
        try {
            await client.query("DELETE FROM annotation WHERE image_filename = ($1);", [image_filename]);
        }
        catch {
            pool.query("ROLLBACK;");
            throw error;
        }

        // iterate through all annotations provided in POST request and insert them in annotation table
        for (let i = 0; i < num_annotations; ++i) {
            try {
                await client.query("INSERT INTO annotation(annotation_id, points, mod_time, curator_id, image_filename, label_name, class_name) VALUES (DEFAULT,($1), current_timestamp, ($2), ($3), ($4), ($5));", [JSON.stringify(annotations[i].points), curator_id, image_filename, annotations[i].label, annotations[i].class]);
            }
            catch {
                await client.query("ROLLBACK;");
                throw error;
            }
        }

        await client.query("COMMIT;"); //finalise transaction
        client.release();
        res.json({ "response": "success" });
    }
    catch (error) {
        res.json({ "response": "error", "message": error });
    }
}

router.post('/api/annotations', authenticateJWT, storePoints);
router.get('/api/annotations/:image_filename', authenticateJWT, db.getAnnotations);



/*** Machine Learning API Routes ***/

router.get('/ml_connection/progress', authenticateJWT, mlServer.getProgress, (req, res) => {
    res.json(req.body);
});

router.post('/ml_connection/detect', authenticateJWT, mlServer.detect, (req, res) => {
    res.json(req.body);
});

router.post('/ml_connection/train', authenticateJWT, mlServer.startTraining, (req, res) => {
    res.json(req.body);
});



/*** Image Server Routes ***/

router.post('/image_server/upload', authenticateJWT, imageServer.uploadImage, (req, res) => {
    res.json(req.body);
});

router.get('/image_server/get_image/:filename', authenticateJWT, imageServer.getImage, (req, res) => {
    res.json(req.body);
});


router.get('/image_server/get_thumbnail/:filename', authenticateJWT, imageServer.getThumbnail, (req, res) => {
    res.json(req.body);
});


module.exports = router;