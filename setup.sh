sudo apt update -y

sudo apt-get install -y docker-compose

# Install Machine Learning API
cd ML_API
sudo chmod +x setup.sh
./setup.sh
cd ..

# Install Image Server
cd Image_Server_API
sudo chmod +x setup.sh
./setup.sh
cd ..

# Install UI and Gateway API
cd UI_Gateway_API
sudo chmod +x setup.sh
./setup.sh
cd ..
