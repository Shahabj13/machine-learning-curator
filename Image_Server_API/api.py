# Main Author: Jack Collins

from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
from PIL import Image

import os
import base64, binascii
import cv2
import io
import numpy

UPLOAD_FOLDER = "/images/"
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'bmp', 'tif', 'tiff', 'webp'}
THUMBNAIL_NAME = "t_"

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


# Take in base64 string and return cv image
def string_to_RGB(base64_decoded_string):
    image = Image.open(io.BytesIO(base64_decoded_string))
    return cv2.cvtColor(numpy.array(image), cv2.COLOR_BGR2RGB)


# check if image file type and extension match
def valid_image(user_ext, base64_decoded_string):
    img = Image.open(io.BytesIO(base64_decoded_string))
    return user_ext.lower() == img.format.lower()

# check if file with same filename (not taking into consideration the extension)
# exists in the upload folder - works best for our current setup in UI
# where filename minus extension is a unique hash
# Also more efficient in case of hash usage O(n) since check only requires one pass of
# upload folder listing
def file_exists(filename):
    separated_filename = filename.split('.')
    return any(File.startswith(separated_filename[0]) for File in os.listdir(UPLOAD_FOLDER))

# create a thumbnail sized image and return it
def write_thumbnail_image(filename):
    tfile_name = THUMBNAIL_NAME + filename
    try:
        new_thumbnail = Image.open(UPLOAD_FOLDER + filename)
        new_thumbnail.thumbnail((200,200))
        new_thumbnail.save(UPLOAD_FOLDER + tfile_name)
        return True
    except IOError:
        return False


# retrieve an image given it's filename
def retrieve_image(filename):
    file = UPLOAD_FOLDER + filename
    image_encoded = ""

    if filename == secure_filename(filename):
        if os.path.exists(file):
            try:
                with open(file, "rb") as fp:
                    image_encoded = base64.b64encode(fp.read()).decode('utf-8')
            except IOError:
                result = 1
            result = 0
        else:
            result = 2
    else:
        result = 3

    return result, image_encoded


# retrieve an image in base64
@app.route('/images/<filename>', methods=['GET'])
def download(filename):
    result, image_encoded = retrieve_image(filename)

    # no switch case in python :/
    if result == 0:
        response = {"response": "success","image": str(image_encoded)}
    elif result == 1:
        response = {"response":"error","message": "file io error"}
    elif result == 2:
        response = {"response":"error","message": "file does not exist"}
    elif result == 3:
        response = {"response":"error","message":"insecure filename used"}

    return jsonify(response)


# retrieve a thumbnail in base64
@app.route('/images/thumbnail/<filename>', methods=['GET'])
def download_thumbnail(filename):
    t_file = THUMBNAIL_NAME + filename
    result, image_encoded = retrieve_image(t_file)

    if result == 0:
        response = {"response": "success","image": str(image_encoded)}
    elif result == 1:
        response = {"response":"error","message": "file io error"}
    elif result == 2:
        response = {"response":"error","message": "file does not exist"}
    elif result == 3:
        response = {"response":"error","message":"insecure filename used"}

    return jsonify(response)


# upload an image
@app.route('/upload', methods=['POST'])
def upload():
    data = request.get_json()
    filename = data['filename']
    user_b64_string = data['image']
    
    if filename == secure_filename(filename):
        try:
            base64d = base64.b64decode(user_b64_string, validate=True) #check if valid b64
        except binascii.Error:
            return jsonify({"response":"error","message":"invalid base64 string"})

        # user shouldn't be able to upload a .jpg if a .jpeg already exists (same file)
        if file_exists(filename):
            return jsonify({"response":"error","message":"file already exists"})

        # get the file extension the user is claiming
        _, user_file_ext = os.path.splitext(filename)
        user_file_ext = user_file_ext[1:]

        # treat jpg and jpeg the same for file extension checks
        # treat tif and tiff for same reason as jpg/jpeg
        if user_file_ext == "jpg":
            user_file_ext = "jpeg"
        elif user_file_ext == "tif":
            user_file_ext = "tiff"

        # file is still uploaded with the extension that the user submits
        # e.g., if user submits a .jpg file it will upload as .jpg and not .jpeg
        if valid_image(user_file_ext, base64d):
            if user_file_ext in ALLOWED_EXTENSIONS:
                img = string_to_RGB(base64d)
                image_result = cv2.imwrite(UPLOAD_FOLDER + filename, img)

                thumbnail_result = write_thumbnail_image(filename)

                if image_result and thumbnail_result:
                    response = {"response":"success","message":"image uploaded successfully"}
                else:
                    response = {"response":"error","message":"image failed to upload"}

                    #ensure either both image and thumbnail exist or none at all
                    if image_result:
                        os.remove(UPLOAD_FOLDER + filename)
                    elif thumbnail_result:
                        os.remove(UPLOAD_FOLDER + THUMBNAIL_NAME + filename)
            else:
                response = {"response":"error","message":"file extension is not allowed"}
        else:
            response = {"response":"error","message":"file extension mismatch between filename extension and image"}
    else:
        response = {"response":"error","message":"insecure filename used"}

    return jsonify(response)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)