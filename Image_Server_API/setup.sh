#!/bin/bash

# Set the color variable
GREEN='\033[0;32m'

echo -e "Creating /images directory if it doesnt already exist..."
mkdir -p /images
echo -e "Building image..."
sudo docker build -t image_server .
echo -e "Running container..."
sudo docker run -d --name image_server -p 8000:8000 -v /images:/images image_server
echo -e "${GREEN}Image server container is now running."