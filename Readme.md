# ***********  ShieldTec Annotation System *********** #

# What is the ShieldTec Annotation System?

The ShieldTec Annotation System (SAS) is a web-based application that provide a user-friendly user interface for the annotation of images and the creation of machine learning datasets. It includes a UI component, a machine learning module based on YOLOv5 for auto-annotation of images, and an image server for the storage of uploaded image files.

# How to setup the system

** Note: This system is only compatible with Linux operating systems. It has only been thoroughly tested on Ubuntu 20.04, and while compatibility with alternative distributions is probable, it cannot be guaranteed. **

** Note: With this approach, if you want to use a different user and password to connect for the annotation database, you will have to change these credentials in the .env file for the UI. **

** Note: If retraining or fine-tuning the machine learning model with a large dataset is not working, try increasing the Docker container's shared memory from the default of 4GB by changing the "--shm-size=4g" parameter in the setup.sh script under ML_API/ **

** Note: The Kubernetes component exists as a prototype and is entirely optional. It can be installed via the README file under kubernetes_autoscaling/ **

After changing its permissions to support execution, install the system by running the setup.sh script as root (sudo ./setup.sh).

During the execution of the script, respond to any prompts to configure the system options.

Once the script has completed its execution, the application will run the UI on port 4056, with the image server accessible from port 8000 and the machine learning API accessible from port 9000.

Once stopped, the system can be started again by running "sudo npm run dev" from within the UI_Gateway_API directory.
The image server can be stopped with "sudo docker container stop image_server", and started again with "sudo docker container start image_server".
The machine learning API can be stopped with "sudo docker container stop ml_api" and started again with "sudo docker container start ml_api".

To install the system modules on separate hosts, run their individual 'setup.sh' scripts on each respective host.

# ***********  UI and API Gateway *********** #

# How to use user interface

First a user must register their details. After successful registration, the user can login to the application.

Upon first login, the settings page will appear. This can be used to specify the network location of the machine learning API and image server. To prevent this page from showing after login, uncheck the relevant checkbox.

Single or multiple image files can be uploaded with the "Upload Image" button. These will be stored on the image server immediately after upload.

To navigate between the upload images, click their thumbnail in the left-hand sidepanel or use the arrow keys.

Before annotating the images, the user should define a series of relevant classes and labels using the class/label creation input box. For instance, if the user is annotating a series of animals, the user might define an "animal" class, with a "dog" and "cat" label. These classes and labels can be deleted under the "Manage Classes" or "Manage Labels" dropdowns.

To manually annotate the images, the relevant class and label should be selected underneath the center canvas. The rectangular or polygonal selection tools can then be used to draw annotations on the canvas. If drawing a polygon, complete the drawing by pressing the "ENTER" key.

To zoom in or out while annotating, using the mouse wheel. Pan and drag the image by clicking and dragging with the selection tool. Reset the zoom and pan using the 'reset zoom' button (magnifying glass).

While drawing a polygon, delete the most recent point by pressing the "DELETE" or "BACKSPACE" key. Once a polygon or box has been drawn, it can be deleted by pressing the "DELETE" or "BACKSPACE" key. To delete only a single point, use the selection tool to click a point before pressing the "DELETE" or "BACKSPACE" key.

Completed annotations can be viewed under the "Manage Annotations" dropdown on the right-hand side of the interface. These can be deleted or hidden individually using their respective buttons. To delete all annotations from the canvas, use the red bin button.

To annotate an image automatically, use the fast-forward icon in the center of the annotation toolbar. This will send the image to the machine learning API, which will return automatically-generated annotations (if any are found).

After annotating an image, click "save" to save the annotations to the database.

To delete images and annotations, select the image thumbnail in the left-hand sidepanel and click the 'Delete' button.

To select multiple thumbnails, click the checkbox icon in the left-hand sidepanel. Thumbnails can then be selected individually, or by holding "SHIFT" and clicking further down the list to select a range.

To retrain the machine learning model, select the thumbnails for the images and annotations that should be used. Then click the "Retrain" button, and specify the number of epochs to train for. The status of this training can be viewed using the "Status" button.

To finetune the machine learning model, follow the same instructions for retraining but select the "Fine-tune" button instead of "Retrain". This will combine the selected images with the historic training dataset and use transfer learning to improve the accuracy of the model or add new object detections to its capabilities.


# ***********  Image Server *********** #

# Routes for API

## GET /images/<filename>
Send GET HTTP request with filename in the URL to retrieve the base64 string in the following JSON string literal:


	{

		"response": "success"/"error",
		
		"image": "image as a base64 string"
		
	}


In the event that the image cannot be found, the JSON string will be the following:


	{

		"response": "success"/"error",
		
		"message": "message value"
		
	}


message value will be one of the following:

1. "file io error"

2. "file does not exist"

3. "insecure filename used"


## GET /images/thumbnail/<filename>
Send GET HTTP request with filename in the URL to retrieve the base64 string of the thumbnail for that image in the following JSON string literal:


	{

		"response": "success/error",
		
		"image": "thumbnail of image as a base64 string"
		
	}


In the event that the image thumbnail cannot be found, the JSON string will be the following:


	{

		"response": "success"/"error",
		
		"message": "message value"
		
	}


message value will be one of the following:

1. "file io error"

2. "file does not exist"

3. "insecure filename used"


## POST /upload
Send POST HTTP request with the following JSON string literal in the body of the request:


	{

		"filename": "filename e.g., cat.jpeg",
		
		"image": "image as a base64 string"
		
	}


The response to this request will be a HTTP request with the following JSON string literal in the body of the request:

	{

		"response": "success"/"error",
	
		"message": "message value"
	
	}


message value will be one of the following:

1. "image uploaded successfully"

2. "filename already exists"

3. "image failed to upload"

4. "file extension is not allowed"

5. "file extension mismatch between filename extension and image"

6. "insecure filename used"

7. "invalid base64 string"


# ***********  Machine Learning API *********** #

# Routes for API

## GET /status
Send HTTP GET request to the /status endpoint to retrieve the training status of the API. The possible responses are:

1.  {"status":"running"} **- training is occurring**

2.  {"status":"stopped"} **- no training is occurring**

## GET /progress
Send HTTP GET request to the /progress endpoint to retrieve the progress of the API's training. The possible responses are:

1.  {"progress":"Epoch: *[epoch]* of *[total-epochs]*, Batch *[batch]* of *[total-batches]*"} **- batch progress for current training epoch**

2.  {"progress":"stopped"} **- no training is occurring**

## POST /detect
Send HTTP POST request with the following JSON to receive detect object auto-annotations:


	{
	  "image": "*[base64-encoded-image]*"
	}


The possible responses are:

1.  {"response": "success", "message": "inference complete", "annotations": [{"label": "**[label]**", "confidence": "**[confidence]**", "x": "**[x]**", "y": "**[y]**", "w": "**[w]**", "h": "**[h]**"}]} **- object annotations**

2.  {"response":"error", "message":"no model available", "image":{}} **- no model exists to use for inference**

3.  {"response":"error", "message":"no objects detected", "image":{"annotations": []}} **- no object detected by inference**

## POST /train
Send HTTP POST request with the following JSON to train the ML model. Multiple images and annotations can be given.

The full path to the image server resource should be specified, e.g. "http://127.0.0.1:8000/images/".

The bounding box coordinates should be given as a string, e.g. "0.6202205882 0.556875 0.027205882352941 0.04375".

The epochs are used to specify the number (an integer) of training epochs (full iterations of the dataset) to perform.

The finetune boolean can be set to true ("finetune": true) to finetune an existing model, or false to train from scratch. If set to true, the API will attempt to combine the given data with the images and annotations used to previously train the model, and will freeze the backbone of the existing checkpoints to only train the higher layers.

	{
	  "image_server": "**[image-server-url]**",
	  "epochs": **[number-of-epochs]**,
	  "finetune": **[finetune-boolean]**,
	  "images":
	  [
		{
		  "filename": "**[image-filename]**",
		  "annotations":
		  [
			{
			  "label": "**[label-name]**",
			  "bounding_box": "**[bounding-box-coordinates]**"
			}
		  ]
		}
	  ]
	}


The possible responses are:

1.  {"response":"error","message":"training already running"} **- training is already occurring, so it cannot start**

2.  {"response":"error","message":"no images could be downloaded"} **- none of the specified images could be downloaded from the image server**

3.  {"response":"success","message":"training initiated"} **- ML model training started**

4.  {"response":"success","message":"training initiated"} **- ML model finetuning started**